files = ["dds_mockup.vhd",
         "wb_clock_monitor.vhd",
         "clock_monitor_wb.vhd",
         "clock_monitor_wbgen2_pkg.vhd",
         "uart_mux.vhd",
         "10mhz_align_unit_wbgen2_pkg.vhd",
         "10mhz_align_unit_wb.vhd",
         "10mhz_align_unit.vhd",
         "fine_pulse_sampler_kintex7.vhd"
         ]

modules = {"local": ["../ip_cores/RFFrameTransceiver"]}
