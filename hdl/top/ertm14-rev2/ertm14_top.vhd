
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.gencores_pkg.all;
use work.wishbone_pkg.all;
use work.wr_fabric_pkg.all;
use work.wrcore_pkg.all;
use work.streamers_pkg.all;
use work.RFFrameTransceiver_pkg.all;
use work.wr_board_pkg.all;
use work.buildinfo_pkg.all;


library unisim;
use unisim.VCOMPONENTS.all;



entity ertm14_top is
  generic(
    g_simulation  : integer := 0;
    g_empty_design_for_clock_eval : integer := 1;
    g_dpram_size  : integer := 131072/4;
    g_dpram_initf : string  := "../../../../../bin/wrc-bootloader.ram";
    g_with_wr_phy : integer := 1
    );
  port (
    -- Clocks

    -- 62.5MHz + offset clock for DDMTD (from IC25/CDCM62002 on eRTM14)
    clk_dmtd_p_i : in std_logic;
    clk_dmtd_n_i : in std_logic;

    -- 125 MHz Clock for Xilinx GTX transceiver (MGTREFCLK0_116)
    -- Output 5 of AD9516 (IC22 on eRTM14)
    gtx_ref_p_i : in std_logic;
    gtx_ref_n_i : in std_logic;

    -- 62.5 MHz WR reference clock from -> change to 125 MHz!
    -- output 9 of AD9516 (IC22 on eRTM14)
    clk_ref_p_i : in std_logic;
    clk_ref_n_i : in std_logic;

    -- 62.5 MHz system clock from
    -- Output 7 of AD9516 (IC22 on eRTM14)
    clk_sys_p_i : in std_logic;
    clk_sys_n_i : in std_logic;

    -- 10 MHz external reference clock (IC27 on eRTM14)
    clk_ext_10m_p_i : in std_logic;
    clk_ext_10m_n_i : in std_logic;

    -- 10/100 MHz feedback from the LTC6950 PLL (used to rephase the 6950 10
    -- MHz output to PPS)
--   clk_pll_10m_fb_p_i : in std_logic;
--   clk_pll_10m_fb_n_i : in std_logic;

    
    -- 62.5 MHz external reference clock (IC23 on eRTM14 - the above * 6.25)
    clk_ext_62m_p_i : in std_logic;
    clk_ext_62m_n_i : in std_logic;

    clk_mtca_a_p_i : in std_logic;
    clk_mtca_a_n_i : in std_logic;

    clk_mtca_b_p_i : in std_logic;
    clk_mtca_b_n_i : in std_logic;

    -- dac internal WR timebase
    dac_main_sclk_o   : out std_logic;
    dac_main_din_o    : out std_logic;
    dac_main_sync_n_o : out std_logic;

    -- dac ddmtd offset clock 
    dac_helper_sclk_o   : out std_logic;
    dac_helper_din_o    : out std_logic;
    dac_helper_sync_n_o : out std_logic;

    -- pll internal WR timebase
    pll_main_sync_o  : out std_logic;
    pll_main_cs_n_o  : out std_logic;
    pll_main_sdi_o   : out std_logic;
    pll_main_sdo_i   : in  std_logic;
    pll_main_sclk_o  : out std_logic;
    pll_main_reset_o : out std_logic;
    pll_main_lock_i  : in  std_logic;

    -- pll external reference clock
    pll_ext_sync_o  : out std_logic;
    pll_ext_cs_n_o  : out std_logic;
    pll_ext_sdi_o   : out std_logic;
    pll_ext_sdo_i   : in  std_logic;
    pll_ext_sclk_o  : out std_logic;
    pll_ext_reset_o : out std_logic;
    pll_ext_lock_i  : in  std_logic;



    -- MicroTCA LVDS interface           
    mtca_a_p_i : inout std_logic;
    mtca_a_n_i : inout std_logic;
    mtca_b_p_i : inout std_logic;
    mtca_b_n_i : inout std_logic;
    mtca_c_p_i : inout std_logic;
    mtca_c_n_i : inout std_logic;


    reset_button_n_i : in std_logic := '1';

    main_xo_en_n_o : out std_logic;

    mac_addr_sda_b : inout std_logic;
    mac_addr_scl_o : inout std_logic;


    --- daughterboard board to board interface

    -- daughterboard DDS0  
    dds0_sdo_i : in std_logic;

    dds0_sclk_p_o : out std_logic;
    dds0_sclk_n_o : out std_logic;

    dds0_sdi_p_o : out std_logic;
    dds0_sdi_n_o : out std_logic;

    dds0_sync_clk_p_i : in std_logic;
    dds0_sync_clk_n_i : in std_logic;

    dds0_sync_p_o : out std_logic;
    dds0_sync_n_o : out std_logic;

    dds0_ioupdate_p_o : out std_logic;
    dds0_ioupdate_n_o : out std_logic;

    dds0_profile_o    : out std_logic_vector (2 downto 0);
    dds0_sync_error_i : in  std_logic;
    dds0_reset_n_o    : out std_logic;

    -- daughterboard DDS1  
    dds1_sdo_i : in std_logic;

    dds1_sclk_p_o : out std_logic;
    dds1_sclk_n_o : out std_logic;

    dds1_sdi_p_o : out std_logic;
    dds1_sdi_n_o : out std_logic;

    dds1_sync_clk_p_i : in std_logic;
    dds1_sync_clk_n_i : in std_logic;

    dds1_sync_p_o : out std_logic;
    dds1_sync_n_o : out std_logic;

    dds1_ioupdate_p_o : out std_logic;
    dds1_ioupdate_n_o : out std_logic;

    dds1_profile_o    : out std_logic_vector (2 downto 0);
    dds1_sync_error_i : in  std_logic;
    dds1_reset_n_o    : out std_logic;

    --- eRTM15 CLKA/B distribution control
    clkab_mosi_o : out std_logic;
    clkab_miso_i : in std_logic;
    clkab_sck_o : out std_logic;
    clka_cs_n_o : out std_logic;
    clkb_cs_n_o : out std_logic;
    
    clka_sync_p_o : out std_logic;
    clka_sync_n_o : out std_logic;
    clkb_sync_p_o : out std_logic;
    clkb_sync_n_o : out std_logic;

    -- eRTM15 PLL (LTC6950) clock generation control
    pll_b2b_sclk_o     : out std_logic;
    pll_b2b_sdi_o      : out std_logic;
    pll_b2b_sdo_i      : in  std_logic;
    pll_b2b_ce_pll_o   : out std_logic;
    pll_b2b_sync_o     : out std_logic;

    dac_b2b_din_o   : out std_logic;
    dac_b2b_sclk_o  : out std_logic;
    dac_b2b_reset_n : out std_logic;    -- rewired to CS_N


    -- eRTM15 LO RF output control
    lo_ctrl_ser_o     : out std_logic;
    lo_ctrl_updtclk_o : out std_logic;
    lo_ctrl_shftclk_o : out std_logic;

    -- eRTM15 REF RF output control
    ref_ctrl_ser_o     : out std_logic;
    ref_ctrl_updtclk_o : out std_logic;
    ref_ctrl_shftclk_o : out std_logic;
  
    -- eRTM15 LEDs
    led_ctrl_ser_o     : out std_logic;
    led_ctrl_updtclk_o : out std_logic;
    led_ctrl_shftclk_o : out std_logic;


    -- daugherboard RF ADC for power monitoring
    pwr_b2b_sclk_o : out std_logic;
    pwr_b2b_dout_i : in  std_logic;
    pwr_b2b_din_o  : out std_logic;
    pwr_b2b_cs_n_o : out std_logic;  -- used pin T19 (CAL_DDS_CONTROL.profile0)

    --  daugherboard management link
    mmc15_tx_to_fpga_i :in std_logic;
    mmc15_int_to_fpga_i :in std_logic;
    mmc15_rx_from_fpga_o : out std_logic;

    --  MMC management link
    mmc14_tx_to_fpga_i :in std_logic;
    mmc14_int_to_fpga_i :in std_logic;
    mmc14_rx_from_fpga_o : out std_logic;

    led_sync_red_o   : out std_logic;
    led_sync_green_o : out std_logic;
    
    -- daughterboard presence signals
    presence_b2b_a1_i : in  std_logic;
    presence_b2b_a2_i : in  std_logic;
    presence_b2b_b1_i : in  std_logic;
    presence_b2b_b2_i : in  std_logic;
    -------------------------------------------------------------------------
    -- SFP pins
    -------------------------------------------------------------------------
    sfp0_txp_o        : out std_logic;
    sfp0_txn_o        : out std_logic;

    sfp0_rxp_i : in std_logic;
    sfp0_rxn_i : in std_logic;

    sfp0_mod_def0_b   : in    std_logic;  -- sfp detect
    sfp0_mod_def1_b   : inout std_logic;  -- scl
    sfp0_mod_def2_b   : inout std_logic;  -- sda
    sfp0_tx_disable_o : out   std_logic;
    sfp0_los_i        : in    std_logic := '0';
    sfp0_led_act_o      : out   std_logic;
    sfp0_led_link_o     : out   std_logic;


    -- sfp1_txp_o : out std_logic;
    -- sfp1_txn_o : out std_logic;

    -- sfp1_rxp_i : in std_logic;
    -- sfp1_rxn_i : in std_logic;

    sfp1_mod_def0_b   : in    std_logic;  -- sfp detect
    sfp1_mod_def1_b   : inout std_logic;  -- scl
    sfp1_mod_def2_b   : inout std_logic;  -- sda
    sfp1_tx_disable_o : out   std_logic;
    sfp1_los_i        : in    std_logic := '0';
    sfp1_led_act_o      : out   std_logic;
    sfp1_led_link_o     : out   std_logic;

    -----------------------------------------
    --UART USB (Front panel)
    -----------------------------------------

    fp_uart_rxd_i : in  std_logic;
    fp_uart_txd_o : out std_logic;
    fp_uart_dtr_i : in  std_logic;

    dbg_uart_rxd_i : in  std_logic; -- Debug Uart RXD pin (FP_AUX_SDA J11 pin 2)
    dbg_uart_txd_o : out std_logic; -- Debug Uart TXD pin (FP_AUX_SCL J11 pin 3)

    -----------------------------------------
    -- PPS IN & OUT
    -----------------------------------------
    pps_i : in  std_logic;
    pps_o : out std_logic;

    flash_miso_i: in std_logic;
    flash_mosi_o: inout std_logic;
    flash_cs_n_o: inout std_logic
    
    
    
    -- synthesis translate_off
;
    sim_wb_i : in  t_wishbone_slave_in;
    sim_wb_o : out t_wishbone_slave_out
   -- synthesis translate_on
    );

end entity ertm14_top;

architecture rtl of ertm14_top is

  constant c_pcs_data_width : integer := 16;


  ------------------------------------------------------------------------------
  -- Components declaration
  ------------------------------------------------------------------------------


  component wr_gtx_phy_kintex7
    generic(
      g_simulation : integer := 0);
    port (
      clk_gtx_i      : in  std_logic;
      qpll_clk_I     : in  std_logic;
      qpll_refclk_i  : in  std_logic;
      qpll_lockdet_i : in  std_logic;
      tx_data_i      : in  std_logic_vector(c_pcs_data_width-1 downto 0);
      tx_k_i         : in  std_logic_vector(c_pcs_data_width/8-1 downto 0);
      tx_disparity_o : out std_logic;
      tx_enc_err_o   : out std_logic;
      rx_rbclk_o     : out std_logic;
      rx_data_o      : out std_logic_vector(c_pcs_data_width-1 downto 0);
      rx_k_o         : out std_logic_vector(c_pcs_data_width/8-1 downto 0);
      rx_enc_err_o   : out std_logic;
      rx_bitslide_o  : out std_logic_vector(4 downto 0);
      rst_i          : in  std_logic;
      loopen_i       : in  std_logic_vector(2 downto 0);
      pad_txn_o      : out std_logic;
      pad_txp_o      : out std_logic;
      pad_rxn_i      : in  std_logic := '0';
      pad_rxp_i      : in  std_logic := '0';
      tx_out_clk_o   : out std_logic;
      tx_locked_o    : out std_logic;
      tx_prbs_sel_i  : in  std_logic_vector(2 downto 0);
      rdy_o          : out std_logic);
  end component wr_gtx_phy_kintex7;



  ------------------------------------------------------------------------------
  -- Signals declaration
  ------------------------------------------------------------------------------

  -- Dedicated clock for GTP transceiver
  signal clk_gtx_ref           : std_logic;
  signal qpll_clk, qpll_refclk : std_logic;
  signal qpll_lockdet          : std_logic;

  signal pps_p : std_logic;
  signal pps_csync : std_logic;
  
  signal clk_20m_vcxo_buf : std_logic;
  signal clk_125m_pllref  : std_logic;
  signal clk_sys          : std_logic;
  signal clk_dmtd         : std_logic;
  signal clk_125m_ref     : std_logic;

  signal dds_REF_sync_clk, dds_LO_sync_clk : std_logic;

  signal wrc_scl_o : std_logic;
  signal wrc_scl_i : std_logic;
  signal wrc_sda_o : std_logic;
  signal wrc_sda_i : std_logic;
  signal sfp_scl_o : std_logic;
  signal sfp_scl_i : std_logic;
  signal sfp_sda_o : std_logic;
  signal sfp_sda_i : std_logic;

  signal dac_hpll_load_p1_o : std_logic;
  signal dac_dpll_load_p1_o : std_logic;
  signal dac_hpll_data_o    : std_logic_vector(15 downto 0);
  signal dac_dpll_data_o    : std_logic_vector(15 downto 0);

  signal pps     : std_logic;
  signal pps_led : std_logic;

  signal clk_pll_10m_fb_iob : std_logic;
  signal clk_pll_10m_fb_fab : std_logic;

  attribute mark_debug : string;

  signal phy_tx_data      : std_logic_vector(15 downto 0);
  signal phy_tx_k         : std_logic_vector(1 downto 0);
  signal phy_tx_disparity : std_logic;
  signal phy_tx_enc_err   : std_logic;
  signal phy_rx_data      : std_logic_vector(15 downto 0);
  signal phy_rx_rbclk     : std_logic;
  signal phy_rx_k         : std_logic_vector(1 downto 0);
  signal phy_rx_enc_err   : std_logic;
  signal phy_rx_bitslide  : std_logic_vector(3 downto 0);
  signal phy_rst          : std_logic;
  signal phy_loopen       : std_logic;
  --loopen_i determines (7 Series Transceiver User Guide(UG476) Figure 2-23 and Table 2-37):
  --'0' => gtx_loopback = "000" => normal operation
  --'1' => gtx_loopback = "100" => Far-end PMA Loopback
  signal phy_loopen_vec   : std_logic_vector(2 downto 0);
  signal phy_prbs_sel     : std_logic_vector(2 downto 0);
  signal phy_rdy          : std_logic;

  signal wrf_src_out : t_wrf_source_out;
  signal wrf_src_in  : t_wrf_source_in;
  signal wrf_snk_out : t_wrf_sink_out;
  signal wrf_snk_in  : t_wrf_sink_in;

  signal tm_time_valid : std_logic;
  signal tm_tai               : std_logic_vector(39 downto 0);
  signal tm_cycles         : std_logic_vector(27 downto 0);

  
  signal rst_sys_n : std_logic;

  signal owr_en : std_logic_vector(1 downto 0);
  signal owr_i  : std_logic_vector(1 downto 0);

  signal clk_ext            : std_ulogic;
  signal clk_ext_mul        : std_logic;
  signal clk_ext_mul_locked : std_logic;
--  signal clk_ref_div2       : std_logic;

  signal dac_cs_n_o                          : std_logic_vector(1 downto 0);
  signal clk_ref, clk_sys_in : std_logic;
  -- signal link0_gtx_rx, link0_gtx_tx : std_logic;
  -- signal link1_gtx_rx, link1_gtx_tx : std_logic;

  signal clk_mtca_a, clk_mtca_b     : std_logic;
  signal clka_sync, clka_sync_delay : std_logic;
  signal clkb_sync, clkb_sync_delay : std_logic;
  
  signal counter                    : unsigned (31 downto 0);

  signal phy_lpc_ctrl, phy_lpc_stat : std_logic_vector(15 downto 0);
  signal phy_rx_rbclk_sampled        : std_logic;

  signal clk_ref_bufr                                               : std_logic;

  signal clk_sys_select : std_logic := '0';  -- 0 : clk_dmtd, 1 : clk_sys

  constant c_master_wrpc      : integer := 0;

  constant c_slave_gpio    : integer := 0;
  constant c_slave_freqmon : integer := 1;
  constant c_slave_uart_mmc_14    : integer := 2;
  constant c_slave_uart_mmc_15    : integer := 3;
  constant c_slave_dds_sync_unit    : integer := 4;
  constant c_slave_10mhz_align_unit : integer := 5;
  constant c_slave_wr_streamers : integer := 6;
  constant c_slave_wr_rf_transceiver : integer := 7;
  constant c_slave_debug_uart : integer := 8;
  constant c_slave_buildinfo_rom : integer := 9;

  constant c_num_gpio_pins : integer := 128;

  constant c_cnx_slave_ports  : integer := 1;
  constant c_cnx_master_ports : integer := 10;

  signal cnx_slave_in  : t_wishbone_slave_in_array(c_cnx_slave_ports-1 downto 0);
  signal cnx_slave_out : t_wishbone_slave_out_array(c_cnx_slave_ports-1 downto 0);

  signal cnx_master_in  : t_wishbone_master_in_array(c_cnx_master_ports-1 downto 0);
  signal cnx_master_out : t_wishbone_master_out_array(c_cnx_master_ports-1 downto 0);

  constant c_num_freqmon_clocks : integer := 5;

  signal fmon_clk_in : std_logic_vector(c_num_freqmon_clocks-1 downto 0);

  constant c_cfg_base_addr : t_wishbone_address_array(c_cnx_master_ports-1 downto 0) :=
    (c_slave_gpio    => x"00000000",
     c_slave_freqmon => x"00000100",
     c_slave_uart_mmc_14    => x"00000200",
     c_slave_uart_mmc_15   => x"00000700",
     c_slave_dds_sync_unit    => x"00000300",
     c_slave_10mhz_align_unit => x"00000400",
     c_slave_wr_rf_transceiver => x"00000500",
     c_slave_wr_streamers => x"00000600",
     c_slave_debug_uart => x"00000800",
     c_slave_buildinfo_rom => x"00000900"
     );

  constant c_cfg_base_mask : t_wishbone_address_array(c_cnx_master_ports-1 downto 0) :=
    (c_slave_gpio    => x"00000f00",
     c_slave_freqmon => x"00000f00",
     c_slave_uart_mmc_14    => x"00000f00",
     c_slave_uart_mmc_15    => x"00000f00",
     c_slave_dds_sync_unit    => x"00000f00",
     c_slave_10mhz_align_unit => x"00000f00",
     c_slave_wr_rf_transceiver => x"00000f00",
     c_slave_wr_streamers => x"00000f00",
     c_slave_debug_uart => x"00000f00",
     c_slave_buildinfo_rom => x"00000f00"
     );

  signal gpio_out, gpio_in, gpio_oen, gpio_oen_n : std_logic_vector(c_num_gpio_pins-1 downto 0);


  signal gtx_qpll_ref_clk, gtx_qpll_clk, gtx_qpll_locked : std_logic;
  signal gtx_qpll_reset                                  : std_logic;
  signal freq_meter_in                                   : std_logic_vector(7 downto 0);
  signal freq_meter_channel_sel                          : std_logic_vector(3 downto 0);
  signal freq_meter_freq                                 : std_logic_vector(31 downto 0);
  signal freq_meter_valid                                : std_logic;


  signal dac_b2b_sclk_wrc : std_logic;
  signal dac_b2b_din_wrc  : std_logic;
  signal dac_b2b_cs_n_wrc : std_logic;

  signal wrc_uart_txd, wrc_uart_rxd : std_logic;

  signal dummy0 : std_logic;
  
  --signal flash_sck, flash_sck_dir : std_logic;
  signal wrc_spi_sclk, wrc_spi_ncs, wrc_spi_mosi, wrc_spi_miso : std_logic;


  signal clk_sys_select_stb : std_logic;
  signal clk_sys_select_next : std_logic;

  signal dbg_clk_preio : std_logic;

  -- Start signals for wr streamers
  constant g_streamers_op_mode  : t_streamers_op_mode   := TX_AND_RX;
  constant g_tx_streamer_params : t_tx_streamer_params  := c_tx_streamer_params_defaut;
  constant g_rx_streamer_params : t_rx_streamer_params  := c_rx_streamer_params_defaut;

  -- WR Streamers <---> RF Frame
  signal strm_tx_data     : std_logic_vector(c_rx_streamer_params_RF.data_width-1 downto 0);
  signal strm_tx_valid    : std_logic;
  signal strm_tx_dreq     : std_logic;
  signal strm_tx_last_p1  : std_logic;
  signal strm_tx_flush_p1 : std_logic;
  -- rx
  signal strm_rx_data     : std_logic_vector(c_tx_streamer_params_RF.data_width-1 downto 0);
  signal strm_rx_valid    : std_logic;
  signal strm_rx_first_p1 : std_logic;
  signal strm_rx_dreq     : std_logic;
  signal strm_rx_last_p1  : std_logic;

  -- RF Frame <---> RF Frame application
  signal rf_tx_ready            : std_logic;
  signal rf_tx_TransmitFrame_p1 : std_logic;
  signal rf_tx_FrameHeader      : t_RFFrameHeader;
  signal rf_tx_RFmFramePayloads : t_RFmFramePayload;
  signal rf_tx_RFsFramePayloads : t_RFsFramePayload;
  signal rf_rx_FrameHeader      : t_RFFrameHeader;
  signal rf_rx_RFmFramePayloads : t_RFmFramePayload;
  signal rf_rx_RFsFramePayloads : t_RFsFramePayload;
  signal rf_rx_Frame_valid_pX   : std_logic;
  signal rf_rx_Frame_typeID     : std_logic_vector(c_RFtype_ID_size-1 downto 0);

  signal rf_reset_nco : std_logic;

  attribute mark_debug of rf_rx_Frame_valid_pX : signal is "TRUE";
  attribute mark_debug of rf_rx_RFmFramePayloads : signal is "TRUE";
  attribute mark_debug of rf_reset_nco : signal is "TRUE";
  attribute mark_debug of strm_rx_valid : signal is "TRUE";
    
  

  signal aux_rst_n      : std_logic;

  -- Aux diagnostics:
  -- 1) streamers have their own ID not to be used by the users
  -- 2) regardless whether streamers are enabled nor not, application can use diagnostics
  -- 3) if application uses diagnostics, it must specify diag_id > 1, diag_ver should start
  --    with 1.
  -- Application diagnostic words are added after streamer's diagnostics in the array that
  -- goes to/from WRPC


  constant c_streamers_diag_id  : integer := 1;  -- id reserved for streamers
  constant c_streamers_diag_ver : integer := 2;  -- version that will be probably increased
  -- when more diagnostics is added to streamers

  -- final values that go to WRPC generics (depend on configuration)
  constant c_diag_id  : integer := f_pick_diag_val(STREAMERS, c_streamers_diag_id, 0);
  constant c_diag_ver : integer := f_pick_diag_val(STREAMERS, c_streamers_diag_ver, 0);

  constant c_diag_ro_size : integer := f_pick_diag_size(STREAMERS, c_WR_STREAMERS_ARR_SIZE_OUT, 0);
  constant c_diag_rw_size : integer := f_pick_diag_size(STREAMERS, c_WR_STREAMERS_ARR_SIZE_IN, 0);

  -- WR SNMP
  signal aux_diag_in  : t_generic_word_array(c_diag_ro_size-1 downto 0);
  signal aux_diag_out : t_generic_word_array(c_diag_rw_size-1 downto 0);

  signal wrs_tx_cfg_i : t_tx_streamer_cfg                               := c_tx_streamer_cfg_default;
  signal wrs_rx_cfg_i : t_rx_streamer_cfg              := c_rx_streamer_cfg_default;

  attribute mark_debug of aux_diag_in : signal is "TRUE";
  attribute mark_debug of aux_diag_out : signal is "TRUE";
  
  
  -- link state
  signal link_ok      : std_logic;

  signal wb_rf_frame_txrx_out : t_wishbone_slave_out;
  signal wb_rf_frame_txrx_in  : t_wishbone_slave_in;

  signal rst_ref_n : std_logic;

    signal     dds0_sync_prebuf : std_logic;
    signal     dds1_sync_prebuf : std_logic;
    signal     clka_sync_prebuf : std_logic;
    signal     clkb_sync_prebuf : std_logic;
    signal     dds0_ioupdate_prebuf : std_logic;
  signal dds1_ioupdate_prebuf : std_logic;

  signal pps_out_mode : std_logic_vector(2 downto 0);
  signal pps_out_preio : std_logic;

  
begin

  process(rst_sys_n, reset_button_n_i, clk_sys_select_stb, clk_sys_select_next)
  begin
    if rst_sys_n = '0' or reset_button_n_i = '0' then
      clk_sys_select <= '0';
    elsif clk_sys_select_stb = '1' then
      clk_sys_select <= clk_sys_select_next;
    end if;
  end process;

  U_Reset_Gen : entity work.ertm14_reset_gen
    port map(
      clk_sys_i        => clk_sys,
      uart_dtr_i => fp_uart_dtr_i,
      rst_button_n_a_i => reset_button_n_i,
      rst_n_o          => rst_sys_n);


  mtca_a : IBUFDS
    generic map (
      DIFF_TERM    => true,   -- Differential Termination (TRUE/FALSE)
      IBUF_LOW_PWR => false,  -- Low Power = TRUE, High Performance = FALSE
      IOSTANDARD   => "LVDS"            -- Specify the I/O standard
      )                                 -- Specify the output slew rate
    port map (
      O  => open,                       -- Buffer output
      I  => mtca_a_p_i,  -- Diff_p inout (connect directly to top-level port)
      IB => mtca_a_n_i   -- Diff_n inout (connect directly to top-level port)
      );

  mtca_b : IBUFDS
    generic map (
      DIFF_TERM    => true,   -- Differential Termination (TRUE/FALSE)
      IBUF_LOW_PWR => false,  -- Low Power = TRUE, High Performance = FALSE
      IOSTANDARD   => "LVDS"            -- Specify the I/O standard
      )                                 -- Specify the output slew rate
    port map (
      O  => open,                       -- Buffer output
      I  => mtca_b_p_i,  -- Diff_p inout (connect directly to top-level port)
      IB => mtca_b_n_i   -- Diff_n inout (connect directly to top-level port)
      );

  mtca_c : IBUFDS
    generic map (
      DIFF_TERM    => true,   -- Differential Termination (TRUE/FALSE)
      IBUF_LOW_PWR => false,  -- Low Power = TRUE, High Performance = FALSE
      IOSTANDARD   => "LVDS"            -- Specify the I/O standard
      )                                 -- Specify the output slew rate
    port map (
      O  => open,                       -- Buffer output
      I  => mtca_c_p_i,  -- Diff_p inout (connect directly to top-level port)
      IB => mtca_c_n_i   -- Diff_n inout (connect directly to top-level port)
      );

  IBUFDS_inst_dmtd : IBUFGDS
    generic map (
      DIFF_TERM    => true,             -- Differential Termination 
      IBUF_LOW_PWR => false,  -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD   => "LVDS")
    port map (
      O  => clk_dmtd,                   -- Buffer output
      I  => clk_dmtd_p_i,  -- Diff_p buffer input (connect directly to top-level port)
      IB => clk_dmtd_n_i  -- Diff_n buffer input (connect directly to top-level port)
      );


  IBUFDS_inst : IBUFGDS
    generic map (
      DIFF_TERM    => true,
      IBUF_LOW_PWR => false,
      IOSTANDARD   => "LVDS")
    port map (
      O  => clk_ref,
      I  => clk_ref_p_i,
      IB => clk_ref_n_i
      );


  IBUFDS_inst2 : IBUFGDS
    generic map (
      DIFF_TERM    => true,
      IBUF_LOW_PWR => false,
      IOSTANDARD   => "LVDS")
    port map (
      O  => clk_sys_in,
      I  => clk_sys_p_i,
      IB => clk_sys_n_i
      );

   BUFGMUX_CTRL_inst : BUFGMUX_CTRL
     port map (
       O  => clk_sys,                    -- 1-bit output: Clock output
       I1 => clk_sys_in,                 -- 1-bit input: Clock input (S=0)
       I0 => clk_dmtd,                   -- 1-bit input: Clock input (S=1)
       S  => clk_sys_select              -- 1-bit input: Clock select
       );
  
  clk_ext_mul_inst : IBUFDS
    generic map (
      DIFF_TERM    => true,
      IBUF_LOW_PWR => false,
      IOSTANDARD   => "LVDS")
    port map (
      O  => clk_ext_mul,
      I  => clk_ext_62m_p_i,
      IB => clk_ext_62m_n_i
      );

  -- clk_pll_fb_buf_inst : IBUFDS
  --   generic map (
  --     DIFF_TERM    => true,
  --     IBUF_LOW_PWR => false,
  --     IOSTANDARD   => "LVDS")
  --   port map (
  --     O  => clk_pll_10m_fb_iob,
  --     I  => clk_pll_10m_fb_p_i,
  --     IB => clk_pll_10m_fb_n_i
  --     );

  
  IBUFDS_inst5 : IBUFDS
    generic map (
      DIFF_TERM    => true,             -- Differential Termination 
      IBUF_LOW_PWR => false,  -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD   => "LVDS")
    port map (
      O  => clk_ext,                    -- Buffer output
      I  => clk_ext_10m_p_i,  -- Diff_p buffer input (connect directly to top-level port)
      IB => clk_ext_10m_n_i  -- Diff_n buffer input (connect directly to top-level port)
      );

  IBUFDS_inst6 : IBUFDS
    generic map (
      DIFF_TERM    => true,             -- Differential Termination 
      IBUF_LOW_PWR => false,  -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD   => "LVDS")
    port map (
      O  => clk_mtca_a,                 -- Buffer output
      I  => clk_mtca_a_p_i,  -- Diff_p buffer input (connect directly to top-level port)
      IB => clk_mtca_a_n_i  -- Diff_n buffer input (connect directly to top-level port)
      );

  IBUFDS_inst7 : IBUFDS
    generic map (
      DIFF_TERM    => true,             -- Differential Termination 
      IBUF_LOW_PWR => false,  -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD   => "LVDS")
    port map (
      O  => clk_mtca_b,                 -- Buffer output
      I  => clk_mtca_b_p_i,  -- Diff_p buffer input (connect directly to top-level port)
      IB => clk_mtca_b_n_i  -- Diff_n buffer input (connect directly to top-level port)
      );


  -- IBUFDS_inst8 : IBUFDS
  --   generic map (
  --     DIFF_TERM    => true,             -- Differential Termination 
  --     IBUF_LOW_PWR => false,  -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
  --     IOSTANDARD   => "LVDS")
  --   port map (
  --     O  => link0_gtx_tx,               -- Buffer output
  --     I  => link0_gtx_tx_p,  -- Diff_p buffer input (connect directly to top-level port)
  --     IB => link0_gtx_tx_n  -- Diff_n buffer input (connect directly to top-level port)
  --     );

  -- IBUFDS_inst9 : IBUFDS
  --   generic map (
  --     DIFF_TERM    => true,             -- Differential Termination 
  --     IBUF_LOW_PWR => false,  -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
  --     IOSTANDARD   => "LVDS"
  --     )
  --   port map (
  --     O  => link0_gtx_rx,               -- Buffer output
  --     I  => link0_gtx_rx_p,  -- Diff_p buffer input (connect directly to top-level port)
  --     IB => link0_gtx_rx_n  -- Diff_n buffer input (connect directly to top-level port)
  --     );

  -- IBUFDS_inst10 : IBUFDS
  --   generic map (
  --     DIFF_TERM    => true,             -- Differential Termination 
  --     IBUF_LOW_PWR => false,  -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
  --     IOSTANDARD   => "LVDS")
  --   port map (
  --     O  => link1_gtx_tx,               -- Buffer output
  --     I  => link1_gtx_tx_p,  -- Diff_p buffer input (connect directly to top-level port)
  --     IB => link1_gtx_tx_n  -- Diff_n buffer input (connect directly to top-level port)
  --     );

  -- IBUFDS_inst11 : IBUFDS
  --   generic map (
  --     DIFF_TERM    => true,             -- Differential Termination 
  --     IBUF_LOW_PWR => false,  -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
  --     IOSTANDARD   => "LVDS")
  --   port map (
  --     O  => link1_gtx_rx,               -- Buffer output
  --     I  => link1_gtx_rx_p,  -- Diff_p buffer input (connect directly to top-level port)
  --     IB => link1_gtx_rx_n  -- Diff_n buffer input (connect directly to top-level port)
  --     );



  --   OBUFS_1 : OBUFDS
  --     generic map(
  --     IOSTANDARD => "LVDS",
  --     SLEW       => "FAST")
  --   port map(
  --     O  => clka_sync_p_o,
  --     OB => clka_sync_n_o,
  --     I  => clka_sync_delay);



  -- OBUFS_2 : OBUFDS
  --   generic map(
  --     IOSTANDARD => "LVDS",
  --     SLEW       => "FAST")
  --   port map(
  --     O  => clkb_sync_p_o,
  --     OB => clkb_sync_n_o,
  --     I  => clkb_sync_delay);

  -- ODELAYE2_inst : ODELAYE2
  --   generic map (
  --     CINVCTRL_SEL          => "FALSE",  -- Enable dynamic clock inversion (FALSE, TRUE)
  --     DELAY_SRC             => "ODATAIN",  -- Delay input (ODATAIN, CLKIN)
  --     HIGH_PERFORMANCE_MODE => "TRUE",  -- Reduced jitter ("TRUE"), Reduced power ("FALSE")
  --     ODELAY_TYPE           => "FIXED",  -- FIXED, VARIABLE, VAR_LOAD, VAR_LOAD_PIPE
  --     ODELAY_VALUE          => 7,       -- Output delay tap setting (0-31)
  --     PIPE_SEL              => "FALSE",  -- Select pipelined mode, FALSE, TRUE
  --     REFCLK_FREQUENCY      => 200.0,  -- IDELAYCTRL clock input frequency in MHz (190.0-210.0, 290.0-310.0).
  --     SIGNAL_PATTERN        => "DATA"   -- DATA, CLOCK input signal
  --     )
  --   port map (
  --     CNTVALUEOUT => open,              -- 5-bit output: Counter value output
  --     DATAOUT     => clkb_sync_delay,  -- 1-bit output: Delayed data/clock output
  --     C           => '0',               -- 1-bit input: Clock input
  --     CLKIN       => clk_sys,
  --     CE          => '0',  -- 1-bit input: Active high enable increment/decrement input
  --     CINVCTRL    => '0',  -- 1-bit input: Dynamic clock inversion input
  --     CNTVALUEIN  => "00000",           -- 5-bit input: Counter value input
  --     ODATAIN     => clkb_sync,         -- 1-bit input: Data input from the I/O
  --     INC         => '0',  -- 1-bit input: Increment / Decrement tap delay input
  --     LD          => '0',               -- 1-bit input: Load IDELAY_VALUE input
  --     LDPIPEEN    => '0',  -- 1-bit input: Enables the pipeline register to load data
  --     REGRST      => '0'   -- 1-bit input: Active-high reset tap-delay input
  --     );

  -- ODELAYE2_inst2 : ODELAYE2
  --   generic map (
  --     CINVCTRL_SEL          => "FALSE",  -- Enable dynamic clock inversion (FALSE, TRUE)
  --     DELAY_SRC             => "ODATAIN",  -- Delay input (ODATAIN, CLKIN)
  --     HIGH_PERFORMANCE_MODE => "TRUE",  -- Reduced jitter ("TRUE"), Reduced power ("FALSE")
  --     ODELAY_TYPE           => "FIXED",  -- FIXED, VARIABLE, VAR_LOAD, VAR_LOAD_PIPE
  --     ODELAY_VALUE          => 7,       -- Output delay tap setting (0-31)
  --     PIPE_SEL              => "FALSE",  -- Select pipelined mode, FALSE, TRUE
  --     REFCLK_FREQUENCY      => 200.0,  -- IDELAYCTRL clock input frequency in MHz (190.0-210.0, 290.0-310.0).
  --     SIGNAL_PATTERN        => "DATA"   -- DATA, CLOCK input signal
  --     )
  --   port map (
  --     CNTVALUEOUT => open,              -- 5-bit output: Counter value output
  --     DATAOUT     => clka_sync_delay,  -- 1-bit output: Delayed data/clock output
  --     C           => '0',               -- 1-bit input: Clock input
  --     CLKIN       => clk_sys,
  --     CE          => '0',  -- 1-bit input: Active high enable increment/decrement input
  --     CINVCTRL    => '0',  -- 1-bit input: Dynamic clock inversion input
  --     CNTVALUEIN  => "00000",           -- 5-bit input: Counter value input
  --     ODATAIN     => clka_sync,         -- 1-bit input: Data input from the I/O
  --     INC         => '0',  -- 1-bit input: Increment / Decrement tap delay input
  --     LD          => '0',               -- 1-bit input: Load IDELAY_VALUE input
  --     LDPIPEEN    => '0',  -- 1-bit input: Enables the pipeline register to load data
  --     REGRST      => '0'   -- 1-bit input: Active-high reset tap-delay input
  --     );


  ------------------------------------------------------------------------------
  -- Dedicated clock for GTP
  ------------------------------------------------------------------------------

  cmp_gtx_clk0_buf : IBUFDS_GTE2
    generic map(
      CLKCM_CFG    => true,
      CLKRCV_TRST  => true,
      CLKSWING_CFG => "11")
    port map(
      O     => clk_gtx_ref,
      ODIV2 => open,
      CEB   => '0',
      I     => gtx_ref_p_i,
      IB    => gtx_ref_n_i);


gen_with_phy:  if g_with_wr_phy /= 0 generate
  U_GTX_QPLL : entity work.wr_gtx_phy_kintex7_lp_qpll
    generic map (
      g_simulation => g_simulation)
    port map (
      rst_i          => gtx_qpll_reset,
      clk_gtx_i      => clk_gtx_ref,
      clk_sys_i      => clk_sys,
      locked_o       => gtx_qpll_locked,
      qpll_clk_o     => gtx_qpll_clk,
      qpll_ref_clk_o => gtx_qpll_ref_clk);

--      debug_i        => phy_debug_in,
--      debug_o        => open);

  U_GTX_Link0 : entity work.wr_gtx_phy_kintex7_lp
    generic map(
      g_simulation => g_simulation)
    port map(
      qpll_clk_i         => gtx_qpll_clk,
      qpll_ref_clk_i     => gtx_qpll_ref_clk,
      qpll_locked_i      => gtx_qpll_locked,
      qpll_reset_o       => gtx_qpll_reset,
      clk_dmtd_i         => clk_dmtd,
      clk_ref_i          => clk_ref,
      tx_data_i          => phy_tx_data(c_pcs_data_width-1 downto 0),
      tx_k_i             => phy_tx_k(c_pcs_data_width/8-1 downto 0),
      tx_disparity_o     => phy_tx_disparity,
      tx_enc_err_o       => phy_tx_enc_err,
      rx_rbclk_o         => phy_rx_rbclk,
      rx_data_o          => phy_rx_data(c_pcs_data_width-1 downto 0),
      rx_k_o             => phy_rx_k(c_pcs_data_width/8-1 downto 0),
      rx_enc_err_o       => phy_rx_enc_err,
--      rx_bitslide_o  => phy_rx_bitslide,
      rx_rbclk_sampled_o => phy_rx_rbclk_sampled,
      rst_i              => phy_rst,
      debug_i            => phy_lpc_ctrl,
      debug_o            => phy_lpc_stat,
      loopen_i           => phy_loopen,
      pad_txn_o          => sfp0_txn_o,
      pad_txp_o          => sfp0_txp_o,
      pad_rxn_i          => sfp0_rxn_i,
      pad_rxp_i          => sfp0_rxp_i,
      tx_clk_o           => clk_125m_ref,
      tx_locked_o        => open,
      tx_prbs_sel_i      => "000",
      rdy_o              => phy_rdy
      );
end generate gen_with_phy;
               
  ODDR_inst : ODDR
    generic map(
      DDR_CLK_EDGE => "OPPOSITE_EDGE", -- "OPPOSITE_EDGE" or "SAME_EDGE" 
      INIT => '0',   -- Initial value for Q port ('1' or '0')
      SRTYPE => "SYNC") -- Reset Type ("ASYNC" or "SYNC")
    port map (
      Q => dbg_clk_preio,   -- 1-bit DDR output
      C => phy_rx_rbclk,    -- 1-bit clock input // phy_rx_rbclk_sampled
      CE => '1',  -- 1-bit clock enable input
      D1 => '0',  -- 1-bit data input (positive edge)
      D2 => '1',  -- 1-bit data input (negative edge)
      R => '0',    -- 1-bit reset input
      S => '0'     -- 1-bit set input
      );
  
  
  -- OBUFS_rfu : OBUFDS
  --      generic map(
  --      IOSTANDARD => "LVDS",
  --      SLEW       => "FAST")
  --    port map(
  --      O  => rfu2_p_o,
  --      OB => rfu2_n_o,
  --      I  => dbg_clk_preio);


  U_dds0_int : entity work.dds_mockup
    port map (
      clk_sys => clk_sys,

      gpio_i => gpio_out(27 downto 21),
      gpio_o => gpio_in(27 downto 21),

      dds_sdo_i   => dds0_sdo_i,
      dds_sdi_p_o => dds0_sdi_p_o,
      dds_sdi_n_o => dds0_sdi_n_o,

      dds_sclk_p_o => dds0_sclk_p_o,
      dds_sclk_n_o => dds0_sclk_n_o,

      dds_sync_clk_p_i => dds0_sync_clk_p_i,
      dds_sync_clk_n_i => dds0_sync_clk_n_i,


      dds_profile_o    => dds0_profile_o,
      dds_sync_error_i => dds0_sync_error_i,
      dds_reset_n_o    => dds0_reset_n_o
      );

  U_dds1_int : entity work.dds_mockup
    port map (
      clk_sys => clk_sys,

      gpio_i => gpio_out(34 downto 28),
      gpio_o => gpio_in(34 downto 28),

      dds_sdo_i   => dds1_sdo_i,
      dds_sdi_p_o => dds1_sdi_p_o,
      dds_sdi_n_o => dds1_sdi_n_o,

      dds_sclk_p_o => dds1_sclk_p_o,
      dds_sclk_n_o => dds1_sclk_n_o,



      dds_sync_clk_p_i => dds1_sync_clk_p_i,
      dds_sync_clk_n_i => dds1_sync_clk_n_i,


      dds_profile_o    => dds1_profile_o,
      dds_sync_error_i => dds1_sync_error_i,
      dds_reset_n_o    => dds1_reset_n_o


      );

  
  U_DDSSyncUnit : entity work.xwb_fine_pulse_gen
    generic map
    (
      g_use_odelay => "111100",
      g_num_channels => 6,
      g_use_external_serdes_clock => false,
      g_target_platform => "Kintex7"
    )
    port map (
      clk_sys_i   => clk_sys,
      clk_ref_i   => clk_ref,
      rst_sys_n_i => rst_sys_n,
      pps_p_i     => pps_csync,

      ext_trigger_p_i => rf_reset_nco,
      
      pulse_o(0) => dds0_sync_prebuf,
      pulse_o(1) => dds1_sync_prebuf,
      pulse_o(2) => clka_sync_prebuf,
      pulse_o(3) => clkb_sync_prebuf,
      pulse_o(4) => dds0_ioupdate_prebuf,
      pulse_o(5) => dds1_ioupdate_prebuf,

      slave_i     => cnx_master_out(c_slave_dds_sync_unit),
      slave_o     => cnx_master_in(c_slave_dds_sync_unit));

 
  U_OBuf_DDS0_Sync : OBUFDS
      generic map(
        IOSTANDARD => "LVDS_25",
        SLEW       => "FAST")
      port map(
        O  => dds0_sync_p_o,
        OB => dds0_sync_n_o,
        I  => dds0_sync_prebuf);

  U_OBuf_DDS1_Sync : OBUFDS
      generic map(
        IOSTANDARD => "LVDS_25",
        SLEW       => "FAST")
      port map(
        O  => dds1_sync_p_o,
        OB => dds1_sync_n_o,
        I  => dds1_sync_prebuf);

  U_OBuf_CLKA_Sync : OBUFDS
      generic map(
        IOSTANDARD => "LVDS_25",
        SLEW       => "FAST")
      port map(
        O  => clka_sync_p_o,
        OB => clka_sync_n_o,
        I  => clka_sync_prebuf);

  U_OBuf_CLKB_Sync : OBUFDS
      generic map(
        IOSTANDARD => "LVDS_25",
        SLEW       => "FAST")
      port map(
        O  => clkb_sync_p_o,
        OB => clkb_sync_n_o,
        I  => clkb_sync_prebuf); 


  U_OBuf_DDS0_IOUPDATE : OBUFDS
      generic map(
        IOSTANDARD => "LVDS_25",
        SLEW       => "FAST")
      port map(
        O  => dds0_ioupdate_p_o,
        OB => dds0_ioupdate_n_o,
        I  => dds0_ioupdate_prebuf);

  U_OBuf_DDS1_IOUPDATE : OBUFDS
      generic map(
        IOSTANDARD => "LVDS_25",
        SLEW       => "FAST")
      port map(
        O  => dds1_ioupdate_p_o,
        OB => dds1_ioupdate_n_o,
        I  => dds1_ioupdate_prebuf);
  
  gen_nonempty_design : if g_empty_design_for_clock_eval /= 0 generate
  

  -- U_10MHz_Out_Aligner: entity work.tenmhz_align_unit
  --   port map (
  --     clk_sys_i      => clk_sys,
  --     clk_ref_i      => clk_ref,
  --     rst_sys_n_i    => rst_sys_n,
  --     pps_p_i        => pps_csync,
  --     clk_10mhz_fb_i => clk_pll_10m_fb_iob,
  --     clk_10mhz_fb_o => clk_pll_10m_fb_fab,
  --     slave_i     => cnx_master_out(c_slave_10mhz_align_unit),
  --     slave_o     => cnx_master_in(c_slave_10mhz_align_unit));
  
  sfp0_mod_def1_b <= '0' when sfp_scl_o = '0' else 'Z';
  sfp0_mod_def2_b <= '0' when sfp_sda_o = '0' else 'Z';
  sfp_scl_i       <= sfp0_mod_def1_b;
  sfp_sda_i       <= sfp0_mod_def2_b;

  sfp1_tx_disable_o <= '0';
  sfp0_tx_disable_o <= '0';

  sfp1_LED_ACT_o  <= '0';
  sfp1_LED_LINK_o <= '0';


  process (clk_sys)
  begin
    if rising_edge (clk_sys) then
      counter   <= counter + 1;
      clka_sync <= '0';
      clkb_sync <= '0';
      if counter(counter'length-1) = '1' then
        clkb_sync <= '1';
        clka_sync <= '1';
        counter   <= (others => '0');
      end if;
    end if;
  end process;
  ---

  U_WR_CORE : xwr_core
    generic map(
      g_simulation                     => g_simulation,
      g_with_external_clock_input      => true,
      --
      g_phys_uart                      => true,
      g_with_phys_uart_fifo => true,
      g_phys_uart_tx_fifo_size => 1024,
      g_phys_uart_rx_fifo_size => 1024,
      g_virtual_uart                   => true,
      g_aux_clks                       => 0,
      g_ep_rxbuf_size                  => 1024,
      g_tx_runt_padding                => true,
      g_pcs_16bit                   => true,
      g_dpram_initf                    => g_dpram_initf,
      g_dpram_size                     => (131072+65536)/4,
      g_interface_mode                 => PIPELINED,
      g_address_granularity            => BYTE,
      g_aux_sdb                        => c_wrc_periph3_sdb,
      g_softpll_enable_debugger        => true,
      g_softpll_use_sampled_ref_clocks => true,
      g_vuart_fifo_size                => 1024,
      g_diag_id                 => c_diag_id,
      g_diag_ver                => c_diag_ver,
      g_diag_ro_size            => c_diag_ro_size,
      g_diag_rw_size            => c_diag_rw_size
      )
    port map(
      clk_sys_i              => clk_sys,
      clk_dmtd_i             => clk_dmtd,
      clk_ref_i              => clk_ref,  --clk_125m_ref,
      clk_ext_mul_i          => clk_ext_mul,
      clk_ext_mul_locked_i   => clk_ext_mul_locked,
      clk_ext_i              => clk_ext,
      pps_ext_i              => pps_i,
      rst_n_i                => rst_sys_n,
      dac_hpll_load_p1_o     => dac_hpll_load_p1_o,
      dac_hpll_data_o        => dac_hpll_data_o,
      dac_dpll_load_p1_o     => dac_dpll_load_p1_o,
      dac_dpll_data_o        => dac_dpll_data_o,
      phy_ref_clk_i          => clk_ref,
      phy_tx_data_o          => phy_tx_data(c_pcs_data_width-1 downto 0),
      phy_tx_k_o             => phy_tx_k(c_pcs_data_width/8-1 downto 0),
      phy_tx_disparity_i     => phy_tx_disparity,
      phy_tx_enc_err_i       => phy_tx_enc_err,
      phy_rx_data_i          => phy_rx_data(c_pcs_data_width-1 downto 0),
      phy_rx_rbclk_i         => phy_rx_rbclk,
      phy_rx_k_i             => phy_rx_k(c_pcs_data_width/8-1 downto 0),
      phy_rx_enc_err_i       => phy_rx_enc_err,
      phy_rx_bitslide_i      => "00000",  --phy_rx_bitslide,
      phy_rst_o              => phy_rst,
      phy_rdy_i              => phy_rdy,
      phy_lpc_ctrl_o            => phy_lpc_ctrl,
      phy_lpc_stat_i            => phy_lpc_stat,
      phy_rx_rbclk_sampled_i => phy_rx_rbclk_sampled,
      phy_loopen_o           => phy_loopen,
      phy_loopen_vec_o       => phy_loopen_vec,
      phy_tx_prbs_sel_o      => phy_prbs_sel,
      phy_sfp_tx_fault_i     => '0',      --sfp0_los_i,
      phy_sfp_los_i          => '0',      --sfp0_los_i,
      phy_sfp_tx_disable_o   => open,     --sfp0_tx,
      led_act_o              => sfp0_LED_ACT_o,
      led_link_o             => sfp0_LED_LINK_o,
      sfp_scl_o              => sfp_scl_o,
      sfp_scl_i              => sfp_scl_i,
      sfp_sda_o              => sfp_sda_o,
      sfp_sda_i              => sfp_sda_i,
      sfp_det_i              => sfp0_mod_def0_b,
      spi_sclk_o             => wrc_spi_sclk,
      spi_ncs_o              => wrc_spi_ncs,
      spi_mosi_o             => wrc_spi_mosi,
      spi_miso_i             => wrc_spi_miso,
      uart_rxd_i             => wrc_uart_rxd,
      uart_txd_o             => wrc_uart_txd,
      owr_pwren_o            => open,
      owr_en_o               => owr_en,
      owr_i                  => owr_i,
      -- synthesis translate_off
      slave_i                => sim_wb_i,
      slave_o                => sim_wb_o,
      -- synthesis translate_on
      aux_master_o           => cnx_slave_in(c_master_wrpc),
      aux_master_i           => cnx_slave_out(c_master_wrpc),
      wrf_src_o            => wrf_src_out,
      wrf_src_i            => wrf_src_in,
      wrf_snk_o            => wrf_snk_out,
      wrf_snk_i            => wrf_snk_in,
      timestamps_o           => open,
      timestamps_ack_i       => '1',
      fc_tx_pause_req_i      => '0',
      fc_tx_pause_delay_i    => x"0000",
      fc_tx_pause_ready_o    => open,
      tm_link_up_o           => open,
      tm_dac_value_o         => open,
      tm_dac_wr_o            => open,
      tm_clk_aux_lock_en_i   => (others => '0'),
      tm_clk_aux_locked_o    => open,
      tm_time_valid_o      => tm_time_valid,
      tm_tai_o             => tm_tai,
      tm_cycles_o          => tm_cycles,
      pps_p_o                => pps_p,
      pps_led_o              => pps_led,
      pps_csync_o            => pps_csync,
      rst_aux_n_o          => aux_rst_n,
      link_ok_o            => link_ok,
      aux_diag_i    => aux_diag_in,
      aux_diag_o    => aux_diag_out


      );


  cmp_xwr_streamers : xwr_streamers
    generic map (
      g_streamers_op_mode       => g_streamers_op_mode,
      g_clk_ref_rate            => 62500000,
      g_tx_streamer_params      => c_tx_streamer_params_RF,
      g_rx_streamer_params      => c_rx_streamer_params_RF,
      g_simulation              => g_simulation)
    port map (
      clk_sys_i                 => clk_sys,
      clk_ref_i                 => clk_ref,

      rst_n_i                   => rst_sys_n,
      src_i                     => wrf_snk_out,
      src_o                     => wrf_snk_in,
      snk_i                     => wrf_src_out,
      snk_o                     => wrf_src_in,

      -- REF clock domain
      tx_data_i                 => strm_tx_data,
      tx_valid_i                => strm_tx_valid,
      tx_dreq_o                 => strm_tx_dreq,
      tx_last_p1_i              => strm_tx_last_p1,
      tx_flush_p1_i             => strm_tx_flush_p1,
      rx_first_p1_o             => strm_rx_first_p1,
      rx_last_p1_o              => strm_rx_last_p1,
      rx_data_o                 => strm_rx_data,
      rx_valid_o                => strm_rx_valid,
      rx_dreq_i                 => strm_rx_dreq,

      -- WR timing depends on clk_dsp_62_5 being IN PHASE with the WR ref clock
      -- ensure this by proper clock/PLL routing!!!!
      tm_time_valid_i           => tm_time_valid,
      tm_tai_i                  => tm_tai,
      tm_cycles_i               => tm_cycles,
      link_ok_i                 => link_ok,
      wb_slave_i                => cnx_master_out(c_slave_wr_streamers),
      wb_slave_o                => cnx_master_in(c_slave_wr_streamers),
      snmp_array_o              => aux_diag_in(c_WR_STREAMERS_ARR_SIZE_OUT-1 downto 0),
      snmp_array_i              => aux_diag_out(c_WR_STREAMERS_ARR_SIZE_IN-1 downto 0),
      tx_streamer_cfg_i         => wrs_tx_cfg_i,
      rx_streamer_cfg_i         => wrs_rx_cfg_i);

  U_REF_Reset : gc_reset
    generic map (
      g_clocks    => 1)
    port map (
      free_clk_i => clk_sys,
      locked_i   => rst_sys_n,
      clks_i(0)     => clk_ref,
      rstn_o(0)     => rst_ref_n);
  
  -- aux_diag_in(c_diag_ro_size-1 downto c_WR_STREAMERS_ARR_SIZE_OUT) <= aux_diag_i;
  -- aux_diag_o                                                       <= aux_diag_out(c_diag_rw_size-1 downto c_WR_STREAMERS_ARR_SIZE_IN);

  -- clock transition block for the Wishbone bus controlling the
  -- RFFrameTransceiver - in order to be really deterministic (down to a single
  -- cycle), the data path between the WR streamers and the RF frame Tx/Rx MUST
  -- be clocked with the DSP clock (that is locked to WR)
  U_Sys2DSPClock : xwb_clock_crossing
    port map (
      slave_clk_i    => clk_sys,
      slave_rst_n_i  => rst_sys_n,
      slave_i        => cnx_master_out(c_slave_wr_rf_transceiver),
      slave_o        => cnx_master_in(c_slave_wr_rf_transceiver),

      master_clk_i   => clk_ref,
      master_rst_n_i => rst_ref_n,
      master_i       => wb_rf_frame_txrx_out,
      master_o       => wb_rf_frame_txrx_in);
  
  cmp_RF : RFFrameTransceiver
    generic map(
      g_rx_RFframeType      => c_ID_RF_ALL,  -- accept all types of RF frame - only debugging
      g_use_wb_config       => true,
      g_slave_mode          => PIPELINED,
      g_slave_granularity   => BYTE)
    port map(
      clk_i                 => clk_ref,
      rst_n_i               => rst_ref_n,

      tx_data_o             => strm_tx_data,
      tx_valid_o            => strm_tx_valid,
      tx_dreq_i             => strm_tx_dreq,
      tx_last_p1_o          => strm_tx_last_p1,
      tx_flush_p1_o         => strm_tx_flush_p1,
      rx_data_i             => strm_rx_data,
      rx_valid_i            => strm_rx_valid,
      rx_first_p1_i         => strm_rx_first_p1,
      rx_dreq_o             => strm_rx_dreq,
      rx_last_p1_i          => strm_rx_last_p1,

      rx_RFFrameHeader_o      => rf_rx_FrameHeader,
      rx_RFmFramePayloads_o => rf_rx_RFmFramePayloads,
      rx_RFsFramePayloads_o => rf_rx_RFsFramePayloads,
      rx_Frame_valid_pX_o   => rf_rx_Frame_valid_pX,
      rx_Frame_typeID_o     => rf_rx_Frame_typeID,

      ready_o               => rf_tx_ready,
      tx_TransmitFrame_p1_i => rf_tx_TransmitFrame_p1,
      tx_RFFrameHeader_i      => rf_tx_FrameHeader,
      tx_RFmFramePayloads_i => rf_tx_RFmFramePayloads,
      tx_RFsFramePayloads_i => rf_tx_RFsFramePayloads,

      wb_slave_i            => wb_rf_frame_txrx_in,
      wb_slave_o            => wb_rf_frame_txrx_out
      );

  rf_tx_TransmitFrame_p1 <= '0';

  rf_reset_nco <= rf_rx_RFmFramePayloads.control(0) and rf_rx_Frame_valid_pX; 


  

  U_Main_DAC_eRTM14 : gc_serial_dac
    generic map (
      g_num_data_bits  => 16,
      g_num_extra_bits => 8,
      g_num_cs_select  => 1,
      g_sclk_polarity  => 0)
    port map (
      clk_i         => clk_sys,
      rst_n_i       => rst_sys_n,
      value_i       => dac_dpll_data_o,
      cs_sel_i      => "1",
      load_i        => dac_dpll_load_p1_o,
      sclk_divsel_i => "010",
      dac_cs_n_o(0) => dac_main_sync_n_o,
      dac_sclk_o    => dac_main_sclk_o,
      dac_sdata_o   => dac_main_din_o);

  U_DMTD_DAC : gc_serial_dac
    generic map (
      g_num_data_bits  => 16,
      g_num_extra_bits => 8,
      g_num_cs_select  => 1,
      g_sclk_polarity  => 0)
    port map (
      clk_i         => clk_sys,
      rst_n_i       => rst_sys_n,
      value_i       => dac_hpll_data_o,
      cs_sel_i      => "1",
      load_i        => dac_hpll_load_p1_o,
      sclk_divsel_i => "010",
      dac_cs_n_o(0) => dac_helper_sync_n_o,
      dac_sclk_o    => dac_helper_sclk_o,
      dac_sdata_o   => dac_helper_din_o);


  U_Main_DAC_eRTM15 : gc_serial_dac     -- main 10 MHz VOCXO
    generic map (
      g_num_data_bits  => 16,
      g_num_extra_bits => 8,
      g_num_cs_select  => 1,
      g_sclk_polarity  => 0)
    port map (
      clk_i         => clk_sys,
      rst_n_i       => rst_sys_n,
      value_i       => dac_dpll_data_o,
      cs_sel_i      => "1",
      load_i        => dac_dpll_load_p1_o,
      sclk_divsel_i => "010",
      dac_cs_n_o(0) => dac_b2b_cs_n_wrc,
      dac_sclk_o    => dac_b2b_sclk_wrc,
      dac_sdata_o   => dac_b2b_din_wrc);


  U_Extend_PPS : gc_extend_pulse
    generic map (
      g_width => 10000000)
    port map (
      clk_i      => clk_sys,
      rst_n_i    => rst_sys_n,
      pulse_i    => pps_led,
      extended_o => open);


  U_Intercon : xwb_crossbar
    generic map (
      g_num_masters => c_cnx_slave_ports,
      g_num_slaves  => c_cnx_master_ports,
      g_registered  => true,
      g_address     => c_cfg_base_addr,
      g_mask        => c_cfg_base_mask)
    port map (
      clk_sys_i => clk_sys,
      rst_n_i   => rst_sys_n,
      slave_i   => cnx_slave_in,
      slave_o   => cnx_slave_out,
      master_i  => cnx_master_in,
      master_o  => cnx_master_out);


  U_UART_MMC_14 : entity work.xwb_simple_uart
    generic map (
      g_INTERFACE_MODE      => PIPELINED,
      g_WITH_VIRTUAL_UART   => false,
      g_WITH_PHYSICAL_UART => true,
      g_TX_FIFO_SIZE => 1024,
      g_RX_FIFO_SIZE => 1024,
      g_WITH_PHYSICAL_UART_FIFO => true,
      g_ADDRESS_GRANULARITY => BYTE,
      g_PRESET_BCR        => 7731 -- BCR: 921600 bps @ 62.5 MHz sysclk
      ) 
    port map (
      clk_sys_i  => clk_sys,
      rst_n_i    => rst_sys_n,
      slave_i    => cnx_master_out(c_slave_uart_mmc_14),
      slave_o    => cnx_master_in(c_slave_uart_mmc_14),
      uart_rxd_i => mmc14_tx_to_fpga_i,
      uart_txd_o => mmc14_rx_from_fpga_o);

  U_UART_MMC_15 : entity work.xwb_simple_uart
    generic map (
      g_INTERFACE_MODE      => PIPELINED,
      g_WITH_VIRTUAL_UART   => false,
      g_WITH_PHYSICAL_UART => true,
      g_TX_FIFO_SIZE => 1024,
      g_RX_FIFO_SIZE => 1024,
      g_WITH_PHYSICAL_UART_FIFO => true,
      g_ADDRESS_GRANULARITY => BYTE,
      g_PRESET_BCR        => 7731 -- BCR: 921600 bps @ 62.5 MHz sysclk
      ) 
    port map (
      clk_sys_i  => clk_sys,
      rst_n_i    => rst_sys_n,
      slave_i    => cnx_master_out(c_slave_uart_mmc_15),
      slave_o    => cnx_master_in(c_slave_uart_mmc_15),
      uart_rxd_i => mmc15_tx_to_fpga_i,
      uart_txd_o => mmc15_rx_from_fpga_o);

  U_UART_debug : entity work.xwb_simple_uart
    generic map (
      g_INTERFACE_MODE      => PIPELINED,
      g_WITH_VIRTUAL_UART   => false,
      g_WITH_PHYSICAL_UART => true,
      g_TX_FIFO_SIZE => 1024,
      g_RX_FIFO_SIZE => 1024,
      g_WITH_PHYSICAL_UART_FIFO => true,
      g_ADDRESS_GRANULARITY => BYTE,
      g_PRESET_BCR        => 7731 -- BCR: 921600 bps @ 62.5 MHz sysclk
      ) 
    port map (
      clk_sys_i  => clk_sys,
      rst_n_i    => rst_sys_n,
      slave_i    => cnx_master_out(c_slave_debug_uart),
      slave_o    => cnx_master_in(c_slave_debug_uart),
      uart_rxd_i => dbg_uart_rxd_i,
      uart_txd_o => dbg_uart_txd_o);


  U_BuildInfoROM: entity work.ertm14_buildinfo_rom
    port map (
      clk_sys_i => clk_sys,
      rst_n_i   => rst_sys_n,
      slave_i   => cnx_master_out(c_slave_buildinfo_rom),
      slave_o   => cnx_master_in(c_slave_buildinfo_rom)
      );
  
  fp_uart_txd_o <= wrc_uart_txd;
  wrc_uart_rxd <= fp_uart_rxd_i;


  U_GPIO : xwb_gpio_port
    generic map (
      g_interface_mode         => PIPELINED,
      g_address_granularity    => BYTE,
      g_num_pins               => c_num_gpio_pins,
      g_with_builtin_tristates => false)
    port map (
      clk_sys_i  => clk_sys,
      rst_n_i    => rst_sys_n,
      slave_i    => cnx_master_out(c_slave_gpio),
      slave_o    => cnx_master_in(c_slave_gpio),
      gpio_out_o => gpio_out,
      gpio_in_i  => gpio_in,
      gpio_oen_o => gpio_oen);

  gpio_oen_n <= not gpio_oen;

  pll_main_cs_n_o  <= gpio_out(0);
  pll_main_sdi_o   <= gpio_out(1);
  gpio_in(2)       <= pll_main_sdo_i;
  pll_main_sclk_o  <= gpio_out(3);
  pll_main_reset_o <= gpio_out(4);
  gpio_in(5)       <= pll_main_lock_i;

  pll_ext_cs_n_o  <= gpio_out(6);
  pll_ext_sdi_o   <= gpio_out(7);
  gpio_in(8)      <= pll_ext_sdo_i;
  pll_ext_sclk_o  <= gpio_out(9);
  pll_ext_reset_o <= gpio_out(10);
  gpio_in(11)     <= pll_ext_lock_i;


  IOBUF_1 : IOBUF
    port map (
      O  => gpio_in(13),
      IO => mac_addr_scl_o,
      I  => '0',
      T  => gpio_out(13));              -- T = 0 -> active

  IOBUF_2 : IOBUF
    port map (
      O  => gpio_in(12),
      IO => mac_addr_sda_b,
      I  => '0',
      T  => gpio_out(12));

  main_xo_en_n_o <= gpio_out(14);


  pll_b2b_sclk_o     <= gpio_out(15);
  pll_b2b_sdi_o      <= gpio_out(16);
  gpio_in(17)        <= pll_b2b_sdo_i;
  pll_b2b_ce_pll_o   <= gpio_out(18);
--  pll_b2b_ce_pll_o <= gpio_out(19);
  pll_b2b_sync_o     <= gpio_out(20);

  lo_ctrl_ser_o     <= gpio_out(39);
  lo_ctrl_updtclk_o <= gpio_out(40);
  lo_ctrl_shftclk_o <= gpio_out(41);

  ref_ctrl_ser_o     <= gpio_out(42);
  ref_ctrl_updtclk_o <= gpio_out(43);
  ref_ctrl_shftclk_o <= gpio_out(44);

  pwr_b2b_sclk_o <= gpio_out(45);
  gpio_in(46)    <= pwr_b2b_dout_i;
  pwr_b2b_din_o  <= gpio_out(47);
  pwr_b2b_cs_n_o <= gpio_out(52);


  dac_b2b_din_o   <= gpio_out(49) when gpio_out(48) = '1' else dac_b2b_din_wrc;
  dac_b2b_sclk_o  <= gpio_out(50) when gpio_out(48) = '1' else dac_b2b_sclk_wrc;
  dac_b2b_reset_n <= gpio_out(51) when gpio_out(48) = '1' else dac_b2b_cs_n_wrc;


  U_FreqMonitor : entity work.xwb_clock_monitor
    generic map (
      g_num_clocks => c_num_freqmon_clocks)
    port map (
      rst_n_i   => rst_sys_n,
      clk_sys_i => clk_sys,
      clk_in_i  => fmon_clk_in,
      pps_p1_i  => '0',
      slave_i   => cnx_master_out(c_slave_freqmon),
      slave_o   => cnx_master_in(c_slave_freqmon));

  fmon_clk_in(0) <= clk_dmtd;
  fmon_clk_in(1) <= clk_sys;
  fmon_clk_in(2) <= clk_pll_10m_fb_fab;
  fmon_clk_in(3) <= clk_ref;
  fmon_clk_in(4) <= phy_rx_rbclk;
  

  wrc_spi_miso <= flash_miso_i;
  flash_cs_n_o <= wrc_spi_ncs;
  flash_mosi_o <= wrc_spi_mosi;
  
  
  U_Kintex7_Startup : STARTUPE2
    generic map (
      PROG_USR      => "FALSE",
      SIM_CCLK_FREQ => 0.0)
    port map (
      CFGCLK    => open,
      CFGMCLK   => open,
      EOS       => open,
      PREQ      => open,
      CLK => '0',
      GSR       => '0',
      GTS       => '0',
      KEYCLEARB => '1',
      PACK      => '1',
      USRCCLKO  => wrc_spi_sclk,
      USRCCLKTS => '0', -- always out
      USRDONEO  => '1',
      USRDONETS => '1');


  clkab_mosi_o <= gpio_out(57);
  gpio_in(57) <= clkab_miso_i;
  clkab_sck_o <= gpio_out(58);
  clka_cs_n_o <= gpio_out(59);
  clkb_cs_n_o <= gpio_out(60);
  

  clk_sys_select_stb <= gpio_out(61);
  clk_sys_select_next <= gpio_out(62);

  pps_out_mode <= gpio_out(65) & gpio_out(64) & gpio_out(63);

  led_ctrl_ser_o     <= gpio_out(66);
  led_ctrl_updtclk_o <= gpio_out(67);
  led_ctrl_shftclk_o <= gpio_out(68);
  led_sync_green_o <= gpio_out(69);
  led_sync_red_o <= gpio_out(70);

  
  process(clk_ref)
  begin
    if rising_edge(clk_ref) then

      pps_o <= pps_out_preio;
      
      case pps_out_mode is
        when "000" => pps_out_preio <= pps_p;
        when "001" => pps_out_preio <= rf_rx_Frame_valid_pX;  
        when "010" => pps_out_preio <= strm_rx_valid;
        when "011" => pps_out_preio <= rf_reset_nco;
        when "100" => pps_out_preio <= '0';
        when "101" => pps_out_preio <= '1';
        when others => pps_out_preio <= '0';
      end case;
    end if;
  end process;

  
  end generate gen_nonempty_design;
  
end architecture rtl;


