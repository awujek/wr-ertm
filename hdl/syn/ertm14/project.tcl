create_project ertm14_top ./
set_property part xc7k70tfbg676-2 [current_project]
set_property target_language VHDL [current_project]
set_property top ertm14_top [get_property srcset [current_run]]
set_property ip_repo_paths ../../ip_cores [current_fileset]
source files.tcl
update_compile_order -fileset sources_1
update_compile_order -fileset sim_1
exit
