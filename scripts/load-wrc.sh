#!/bin/bash


D=`dirname $0`

UART_BOOT=$D/../software/wrpc-sw/tools/uart-bootloader/usb-bootloader.py

USB_PORT_WRPC="/dev/ttyUSB2"
USB_PORT_MMC15="/dev/ttyUSB0"
USB_PORT_MMC14="/dev/ttyUSB1"

BIN_DIR=$D/../bin

$UART_BOOT -b default -s 921600 -p $USB_PORT_WRPC -t $BIN_DIR/wrc.bin

