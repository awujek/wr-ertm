//------------------------------------------------------------------------------
// Copyright CERN 2018
//------------------------------------------------------------------------------
// Copyright and related rights are licensed under the Solderpad Hardware
// License, Version 2.0 (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
// http://solderpad.org/licenses/SHL-2.0.
// Unless required by applicable law or agreed to in writing, software,
// hardware and materials distributed under this License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
// or implied. See the License for the specific language governing permissions
// and limitations under the License.
//------------------------------------------------------------------------------

`include "vhd_wishbone_master.svh"

`include "regs/endpoint_regs.vh"
`include "regs/endpoint_mdio.vh"
`include "softpll_regs_ng.vh"

`include "wr_1000basex_phy_model.svh"
`include "regs/rf_frame_transceiver_wb.vh"
`include "regs/insn_uart_regs.vh"

`include "wb_fine_pulse_gen_driver.svh"

`timescale 1ps/1ps

`define MDIO_DBG1_RESET_TX (1 << 0)
`define MDIO_DBG1_TX_ENABLE (1 << 1)
`define MDIO_DBG1_RX_ENABLE (1 << 2)
`define MDIO_DBG1_RESET_RX (1 << 3)
`define MDIO_DBG1_DMTD_SOURCE_TXOUTCLK (1 << 14)
`define MDIO_DBG1_DMTD_SOURCE_RXRECCLK (0 << 14)

`define MDIO_DBG0_RESET_TX_DONE (1 << 0)
`define MDIO_DBG0_LINK_UP (1 << 1)
`define MDIO_DBG0_LINK_ALIGNED (1 << 2)
`define MDIO_DBG0_RESET_RX_DONE (1 << 3)

`define MDIO_DBG1 (19<<2)
`define MDIO_DBG0 (18<<2)

module dupa;
    ertm14_top dut();
endmodule // dupa


class IBusDevice;

   CBusAccessor m_acc;
   uint64_t m_base;

   function new ( CBusAccessor acc, uint64_t base );
      m_acc =acc;
      m_base = base;
   endfunction // new
   
   virtual task write32( uint32_t addr, uint32_t val );
     // $display("write32 addr %x val %x", m_base + addr, val);
      
      m_acc.write(m_base +addr, val);
//      #100ns;
      
   endtask // write
   
   virtual task read32( uint32_t addr, output uint32_t val );
      automatic uint64_t val64;
      
      m_acc.read(m_base + addr, val64);
     // $display("read32 addr %x val %x", m_base + addr, val64);
      val = val64;
//      #100ns;

   endtask // write
endclass // BusDevice

class WREndpointDriver extends IBusDevice;
   function new(CBusAccessor bus, uint64_t base);
      super.new(bus, base);
   endfunction // new

   task automatic mdio_read(int addr, output int val);
      uint32_t rval;

      write32( `ADDR_EP_MDIO_CR, (addr>>2) << 16);
      while(1)begin
	 read32( `ADDR_EP_MDIO_ASR, rval);
	 if(rval & 'h80000000) begin
	    val  = rval[15:0];
	    return;
	 end
      end
   endtask // mdio_read

   task automatic mdio_write(int addr,int val);
      uint32_t rval;

      write32( `ADDR_EP_MDIO_CR, (addr>>2) << 16 | `EP_MDIO_CR_RW | val);
      while(1)begin
	 read32( `ADDR_EP_MDIO_ASR, rval);
	 if(rval & 'h80000000)
	   return;
      end
   endtask // automatic
   
endclass // WREndpointDriver

/*
 
class WRStreamers;

   protected CBusAccessor m_acc;
   protected int m_base;
   function new(CBusAccessor bus, uint64_t base);
      m_acc = bus;
      m_base =base;
   endfunction // new

   task automatic configure();
      // set fixed latency
      m_acc.write(m_base+`ADDR_WR_STREAMERS_RX_CFG5, 750); // in periods of 8ns.
      m_acc.write(m_base+`ADDR_WR_STREAMERS_RX_CFG6, 1000); // in periods of 8ns.
      // enable fixed latency (overide register)
      m_acc.write(m_base+`ADDR_WR_STREAMERS_CFG, 1<<21);
   endtask // configure

   task automatic configure2();
      // set fixed latency
      m_acc.write(m_base+`ADDR_WR_STREAMERS_RX_CFG5, 850); // in periods of 8ns.
      // enable fixed latency (overide register)
      m_acc.write(m_base+`ADDR_WR_STREAMERS_CFG, 1<<21);
   endtask // configure

   
   
endclass // WRStreamers

 */
 
class RFFrameTransceiverDriver extends IBusDevice;
   function new(CBusAccessor bus, uint64_t base);
      super.new(bus, base);
   endfunction // new

   
   task automatic configure();

      write32( `ADDR_RF_TX_PERIOD, 500 );
      write32( `ADDR_RF_SCR, (`RF_SCR_TX_OR_CONFIG) |
               (1 << `RF_SCR_TX_DBG_FTYPE_OFFSET) |
               (1 << `RF_SCR_TX_DBG_OFFSET) );


   endtask // configure
endclass // RFFrameTransceiverDriver


class IUARTDriver extends IBusDevice;

   const uint32_t  ESC_CHAR_VAL               = 'h24;
   const uint32_t  START_MASK_EN              = 'hf0;
   const uint32_t  START_INSN_CHAR_VAL        = 'hf0;
   const uint32_t  START_WRITE_CHAR_VAL       = 'hf1;
   const uint32_t  START_READ_CHAR_VAL        = 'hf2;
   const uint32_t  START_CPL_CHAR_VAL         = 'hf3;
   const uint32_t  START_WRITE_CTRL_CHAR_VAL  = 'hf4;
   const uint32_t  START_READ_CTRL_CHAR_VAL   = 'hf5;
   const uint32_t  START_CPL_CTRL_CHAR_VAL    = 'hf6;
   const uint32_t  END_CHAR_VAL               = 'h45;

  
   function new(CBusAccessor bus, uint64_t base);
      super.new(bus, base);
   endfunction // new

   task automatic enable(int val);
      write32( 'h40 + `ADDR_IUART_UART_EN, val );
   endtask // enable

   task automatic configure();
      enable(0);
      write32( 'h40 +  `ADDR_IUART_ESC_CHAR,         ESC_CHAR_VAL);     
      write32( 'h40 +  `ADDR_IUART_START_INSN_CHAR,  START_INSN_CHAR_VAL);    
      write32( 'h40 +  `ADDR_IUART_START_WRITE_CHAR, START_WRITE_CHAR_VAL);     
      write32( 'h40 +  `ADDR_IUART_START_READ_CHAR,  START_READ_CHAR_VAL);      
      write32( 'h40 +  `ADDR_IUART_START_CPL_CHAR,   START_CPL_CHAR_VAL);
      write32( 'h40 +  `ADDR_IUART_END_CHAR,	     END_CHAR_VAL);
      write32( 'h40 +  `ADDR_IUART_WRITE_CTRL_CHAR,  START_WRITE_CTRL_CHAR_VAL);
      write32( 'h40 +  `ADDR_IUART_READ_CTRL_CHAR,   START_READ_CTRL_CHAR_VAL );
      enable(1);
      
   endtask // configure
   
	

endclass // IUartDriver


      
module main;

   reg rst_n = 0;
   reg clk_125m = 0;
   reg clk_62m5 = 0;
   reg clk_dmtd = 0;
   
   always #4ns clk_125m <= ~clk_125m;
   always #(7.99ns) clk_dmtd <= ~clk_dmtd;
   always @(posedge clk_125m) clk_62m5 <= ~clk_62m5;

   initial begin
      repeat(20) @(posedge clk_125m);
      rst_n = 1;
   end

   wire loop_p, loop_n;
   
   
   // the Device Under Test
   ertm14_top 
    #(
      .g_simulation(1),
      .g_dpram_initf("../../../bin/wrc-sim.bram")
      )
   DUT
     (
      .reset_button_n_i           (rst_n),


      .clk_sys_p_i (clk_62m5),
      .clk_sys_n_i (~clk_62m5),

      .clk_ref_p_i (clk_125m),
      .clk_ref_n_i (~clk_125m),

      .clk_dmtd_p_i (clk_dmtd),
      .clk_dmtd_n_i (~clk_dmtd),

      .gtx_ref_p_i (clk_125m),
      .gtx_ref_n_i (~clk_125m),

      .sim_wb_i            (Host.out),
      .sim_wb_o            (Host.in),

      .sfp0_txp_o(loop_p),
      .sfp0_txn_o(loop_n),
      .sfp0_rxp_i(loop_p),
      .sfp0_rxn_i(loop_n)
      
   );

      
     wr_1000basex_phy_model
     U_Model
       (
	.clk_ref_i(clk_62m5),
	.rst_i(~rst_n),
	.tx_data_i(16'b0),
	.tx_k_i(2'b0),
	.loopen_i(1'b0),
	

	.pad_rxp_i(loop_p),
	.pad_rxn_i(loop_n)
	
	);

   IVHDWishboneMaster Host
     (
      .clk_i   (DUT.clk_sys),
      .rst_n_i (DUT.rst_sys_n));




   
   initial begin
      CBusAccessor acc = Host.get_accessor();
      WREndpointDriver drv = new (acc, 'h20100);
//      WRStreamers strm = new (acc, 'h40700);
      RFFrameTransceiverDriver rftx = new (acc, 'h48500);
      FinePulseGenDriver fpg = new(acc, 'h48300);
      IUARTDriver iuart = new (acc, 'h48200 );
      
      $timeformat (-6, 3, "us", 10);

      @(posedge DUT.rst_sys_n);
      @(posedge DUT.clk_sys);

      
      #20us;

      $error("iuart");
      
      iuart.configure();
      
      
      $error("RFTx");
      rftx.configure();
      #1us;
      
      fpg.pulse(0, 0, 0, 200.0);

      $stop;
      
      
      
//      drv.mdio_write(`MDIO_DBG1, `MDIO_DBG1_RESET_TX);
//      drv.mdio_write(`MDIO_DBG1, 0);


    //  acc.write('h40200 + `ADDR_SPLL_DAC_MAIN, 'hcafe );
      
		
      
      $display("DAC done");
      
      

   end // initial begin

endmodule // main
