library ieee;
use ieee.STD_LOGIC_1164.all;
use ieee.NUMERIC_STD.all;

use work.gencores_pkg.all;

entity ertm14_reset_gen is
  
  port (
    clk_sys_i : in std_logic;

    uart_dtr_i : in std_logic;
    rst_button_n_a_i : in std_logic;

    rst_n_o : out std_logic
    );

end ertm14_reset_gen;

architecture behavioral of ertm14_reset_gen is

  constant c_max_dtr_pulse_width : integer := 6250000;
  constant c_dtr_pulses_to_reset : integer := 10;

  attribute mark_debug : string;

  
  signal powerup_cnt     : unsigned(7 downto 0) := x"00";
  signal button_synced_n : std_logic;
  signal powerup_n       : std_logic            := '0';

  signal uart_edge_p : std_logic;
  signal width_cnt : unsigned(23 downto 0);
  signal pulse_cnt : unsigned(7 downto 0);
  signal uart_reset_n : std_logic;

  attribute mark_debug of uart_edge_p : signal is "TRUE";
  attribute mark_debug of width_cnt : signal is "TRUE";
  attribute mark_debug of pulse_cnt : signal is "TRUE";
  attribute mark_debug of uart_reset_n : signal is "TRUE";
  attribute mark_debug of uart_dtr_i : signal is "TRUE";
  
begin  -- behavioral

  U_EdgeDet_UART : gc_sync_ffs port map (
    clk_i    => clk_sys_i,
    rst_n_i  => '1',
    data_i   => uart_dtr_i,
    ppulse_o => uart_edge_p);

  U_Sync_Button : gc_sync_ffs port map (
    clk_i    => clk_sys_i,
    rst_n_i  => '1',
    data_i   => rst_button_n_a_i,
    synced_o => button_synced_n);

  p_uart_reset : process(clk_sys_i, powerup_n)
  begin
    if powerup_n = '0' then
      width_cnt <= (others => '0');
      pulse_cnt <= (others => '0');
      uart_reset_n <= '1';
    elsif rising_edge(clk_sys_i) then

      uart_reset_n <= '1';
      
      if uart_edge_p = '1' then
        width_cnt <= (others => '0');

        if pulse_cnt = c_dtr_pulses_to_reset then
          uart_reset_n <= '0';
          pulse_cnt <= (others => '0');
        elsif width_cnt /= c_max_dtr_pulse_width then
          pulse_cnt <= pulse_cnt + 1;
        else
          pulse_cnt <= (others => '0');
        end if;

      elsif width_cnt /= c_max_dtr_pulse_width then
        width_cnt <= width_cnt + 1;
      else
        pulse_cnt <= (others => '0');
      end if;
    end if;
  end process;
  
  
  p_powerup_reset : process(clk_sys_i)
  begin
    if rising_edge(clk_sys_i) then
      if(powerup_cnt /= x"ff") then
        powerup_cnt <= powerup_cnt + 1;
        powerup_n   <= '0';
      else
        powerup_n <= '1';
      end if;
    end if;
  end process;

  rst_n_o <= powerup_n and button_synced_n and uart_reset_n;

end behavioral;
