White Rabbit High Peformance Timing Receiver (a.k.a eRTM14/15)
=============================================================

*Warning, this is not an official documentation - just a collection of notes for whoever will be doing the software/firmware development*

Introduction
------------

This repo contains the HDL/software for the White Rabbit High Peformance Timing Receiver (further referenced to as the eRTM).
It's a MTCA.4 eRTM (slots 14-15) WR receiver with redundant uplinks, providing a variety of ultra-low jitter clock signals.
The device is composed by two MTCA eRTM (Rear Transition Module) modules:
- Digital board (WR node), sitting in the slot 14. The board provides a redundant uplink to the WR network (2 SFP ports) and hosts the FPGA that implements the WR stack.
- RF board, using the slot 15. This board produces the Local Oscillator/Reference RF signals using a DDS as well as two configurable digital LVPECL clocks. They are distributed to the RTM/AMC slots of a MTCA.4 crate through the MTCA.4 RF Backplane The two boards always go together (stacked in a "sandwich" form) into the neighboring backplane slots 14 and 15. Inter-board communication is done through a high speed board-to-board cable.

For the schematics/layouts and more detailed description of the device, please go to: https://www.ohwr.org/project/ertm15-llrf-wr/

Official EDMS releases of the schematics/PCB layouts are here:
- EDA-03849 (digital board) https://edms.cern.ch/ui/#!master/navigator/item?P:100145998:100145999:subDocs
- EDA-03850 (analog board) https://edms.cern.ch/ui/#!master/navigator/item?I:100146009:100146009:subDocs


For the programmers
-------------------

There are 4 programmable chips in the whole design:
- Kintex-7 FPGA with the WR Core and all logic necessary to talk with the chips on the board.
- LM32 embedded CPU inside the FPGA that runs the WR Core firmware
- Two Cortex-M3 ARM MCUs (one on each board of the 'sandwich'), called MMCs which provide the IPMI/management functionality required by the MTCA standard.

This repo contains codes for all of them.

Necessary tools
---------------

For the FPGA:
- Xilinx Vivado 2018.2
- Xilinx JTAG cable

For the LM32 (WR Core):
- USB mini cable
- LM32 toolchain (you can get the binaries from OHWR.org)
- Python + PySerial for the USB bootloader

For the MMCs:
- GCC ARM toolchain for Cortex-M3 (arm-none-eabi-gcc from Ubuntu 18.04 works just fine)
- JTAG cable with adapter (tested on STLINK-V2)
- 3.3-volt USB-UART dongle
- OpenOCD for flash programming

Directory structure
-------------------

bin/                              - prebuilt binaries for initialization of the FPGA memories
hdl/                              - FPGA HDL repo
hdl/top                           - top level design RTL
hdl/syn                           - top level design Vivado project

software/ertm14-mmc-stub          - "stub" firmware for the MMCs, which just switches on the power to the entire board (voltage regulators are controlled by the MMC on each board).
software/openMMC                  - openMMC firmware (port of openMMC to STM32)
software/wrpc-sw                  - port of WR PTP Core software for the eRTM14/15 board
software/ipmitool                 - ipmitool hack to provide remote console & SNMP-over-IPMI implementation.


How to build and program this stuff
-----------------------------------

FPGA:
- TODO: .tcl files to recreate the vivado project
- open the project using Vivado
- program the FPGA (either the flash - chose s25fl128s) or the chip directly

LM32
- cd wrpc-sw/
- check out the latest dev branch (tom-dev-ertm14)
- git submodule update --init
- make ertm14_defconfig
- make
- connect the mini-usb cable to the front panel USB connector of eRTM14
- make load PORT=/dev/ttyUSBx (x = USB device representing the eRTM14's USB-to-serial bridge)
- the USB bootloader leaves a serial debugging console open. Press Ctrl-A/Ctrl-D to kill it.

MMCs:
- cd openMMC/
- create two build directories, one for each MMC MCU. Let's call them build-ertm14 and build-ertm15
- cd build-ertm14
- cmake .. -DBOARD=ertm14 -DDEBUG_PROBE=OPENOCD-STLINK-V2
- make
- cd build-ertm15
- cmake .. -DBOARD=ertm15 -DDEBUG_PROBE=OPENOCD-STLINK-V2
- make
- to program: connect the JTAG, execute make program_app in the build directory. Make debug_app leaves the openocd server running, you can connect to it using gdb (port 3333)
- the MMCs have a debug UART, I connected it to two extra pins (TXD, GND, soldered by hand) in the MMC JTAG header. The speed is 115200 8n1, you need a 3.3V uart dongle to use it.

Communication paths (for David):
===============================

IPMI->WRCore (LM32)
-------------------

- The PC sends IPMI requests using send_raw_command()/ertm14_ipmi_transfer() in ipmitool/tools/ertm14-lib/ertm14-lib.c. This is just a wrapper function on top of ipmitool's stuff (with everything preconfigured - e.g. the I2C bridge addresses, etc.). Note that all the traffic is always initiated by the PC side (the board can't send anything on its own)

- The corresponding MMC (in our case, the one on eRTM14) receives the request and calls a custom IPMI request handler, defined in openmmc/modules/fpga_uart.c:

IPMI_HANDLER(ipmi_custom_snmp, NETFN_CUSTOM, 0, ipmi_msg * req, ipmi_msg * rsp )

This handler so far can understand the following requests:
- ERTM14_IUART_MSG_IPMI_REBOOT_FPGA: reboots the FPGA (just wiggles the CONFIG pin)
- ERTM14_MSG_IPMI_COMPLETION: requests a completion of the previous IPMI response, if it exceeds the IPMI payload size limit (30something bytes). I was
forced to implement a crude 'fragmentation/completion' protocol because most SNMP/netconsole responses can't fit in this size limit. This fragmentation occurs in the ipmi_custom_snmp() handler on the MMC side and in ertm14-lib: ertm14_ipmi_transfer() on the host side.

- All other requests (whether it's an SNMP payload or network console payload) are sent over the 'Instruction UART' (a slightly fancier UART with hardware flow control), the other end being the LM32 running the WR Core software. The handler in MMC waits synchronously for the response from IUART. If the response's payload size is larger than the maximum IPMI packet payload, it's truncated and the 1st byte of the payload (packet type) has a flag set informing the host PC there's more to read out (through ERTM14_MSG_IPMI_COMPLETION).

- the IUART on the LM32 receives the request. The WRPC task responsible for the IUART is executed in ertm14.c : iuart_14_poll()

- so far I've only managed to have the netconsole running. This is invoked by ertm14.c:handle_iuart_request(). The console driver (dev/console.c) now provides support for multiple readers/writers, so that the front panel UART traffic can coexist with the network traffic. The entry point to the driver from the remote side is in dev/console.c:console_ipmi_process_request()

- the way of the response packets is exactly opposite - just reverse the direction. Keep in mind that ertm14-lib emulates large packet payloads by splitting/reassembling packets in ertm14_ipmi_transfer().

List of tasks
=============
- do a quick review of my changes to wrpc-sw (esp. the impact on maintenance and merging this with master).
- bring back legacy temperature sensors (I commented out the linker sections - so they need to be explitely registered now)
- bring back remaining shell commands (same reason as above)
- change KConfigs to have board selection instead of raw feature selection (i.e. compiling wrpc-sw for SPEC, SVEC, eRTM instead of compiling wrpc-sw for 8-bit or 16-bit PHY with/without Etherbone support, etc. )
- implement IPMI/SNMP forwarding. On the LM32 it should be fairly straightforward (call snmp.c/snmp_respond() with the IUART payload), on the PC - it depends what FESA people will want to use. I started developing a proxy server (forwards SNMP traffic from a local port to a given eRTM board), so that we could use snmpget/set command line tools - but I'm not sure running such a proxy would be desirable on the frontends. To be discussed with IN/SRC people - IIRC Frank wrote some IPMI support for FESA.
- Implement SNMP hooks for the eRTM14 configuration parameters (I'll write a separate document on what needs to be exposed).
- Implement readout of all temperature sensors on board as well as the OCXO current. The sensors are conencted to the MMCs, which should periodically send the temperatures/currents to the LM32 over the same IUART that is used for IPMI traffic. It would be good if the wrpc-sw could recognize them through the existing temperature sensor library (dev/temperature.c - needs factoring out custom linker sections).
- SNMP test program







