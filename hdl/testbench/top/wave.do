onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/clk_sys_i
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/clk_ref_i
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/rst_n_i
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/src_i
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/src_o
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/snk_i
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/snk_o
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/tx_data_i
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/tx_valid_i
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/tx_dreq_o
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/tx_last_p1_i
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/tx_flush_p1_i
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/rx_first_p1_o
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/rx_last_p1_o
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/rx_data_o
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/rx_valid_o
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/rx_dreq_i
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/rx_late_o
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/rx_timeout_o
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/tm_time_valid_i
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/tm_tai_i
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/tm_cycles_i
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/link_ok_i
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/wb_slave_i
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/wb_slave_o
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/snmp_array_o
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/snmp_array_i
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/tx_streamer_cfg_i
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/rx_streamer_cfg_i
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/to_wb
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/from_wb
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/dbg_word
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/start_bit
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/rx_data
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/wb_regs_slave_in
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/wb_regs_slave_out
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/tx_frame
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/reset_time_tai
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/latency_acc
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/latency_cnt
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/sent_frame_cnt_out
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/rcvd_frame_cnt_out
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/lost_frame_cnt_out
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/lost_block_cnt_out
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/rx_stat_match_cnt
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/rx_stat_timeout_cnt
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/rx_stat_late_cnt
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/rx_valid
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/rx_latency_valid
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/rx_latency
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/rx_lost_frames
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/rx_lost_frames_cnt
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/rx_lost_blocks
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/rx_frame
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/rx_stat_match_p1
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/rx_stat_late_p1
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/rx_stat_timeout_p1
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/tx_streamer_cfg
add wave -noupdate -group Streamers /main/DUT/gen_nonempty_design/cmp_xwr_streamers/rx_streamer_cfg
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/clk_sys_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/clk_ref_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/rst_n_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/src_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/src_o
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tm_time_valid_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tm_tai_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tm_cycles_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/link_ok_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_data_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_valid_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_dreq_o
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_sync_o
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_last_p1_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_flush_p1_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_reset_seq_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_frame_p1_o
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_streamer_cfg_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_threshold_hit
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_timeout_hit
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_flush_latched
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_idle
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_last
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_we
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_full
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_empty
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_rd
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_empty_int
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_rd_int
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_rd_int_d
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_q_int
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_q_reg
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_q_valid
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_q
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_d
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_flush
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_flush_p2
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/state
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/seq_no
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/count
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/ser_count
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/word_count
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/total_words
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/timeout_counter
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/pack_data
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/fsm_out
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/escaper
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/fab_src
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/fsm_escape
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/fsm_escape_enable
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/crc_en
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/crc_en_masked
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/crc_reset
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/crc_value
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_almost_empty
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_almost_full
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/buf_frame_count_inc_ref
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/buf_frame_count_dec_sys
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/buf_frame_count
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tag_cycles
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tag_valid
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tag_valid_latched
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tag_error
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/link_ok_delay_cnt
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/link_ok_delay_expired
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/link_ok_delay_expired_ref
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/link_ok_ref
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/clk_data
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/rst_n_ref
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/stamper_pulse_a
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/rst_int_n
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/clk_sys_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/clk_ref_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/rst_n_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/src_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/src_o
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tm_time_valid_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tm_tai_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tm_cycles_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/link_ok_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_data_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_valid_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_dreq_o
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_sync_o
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_last_p1_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_flush_p1_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_reset_seq_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_frame_p1_o
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_streamer_cfg_i
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_threshold_hit
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_timeout_hit
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_flush_latched
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_idle
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_last
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_we
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_full
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_empty
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_rd
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_empty_int
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_rd_int
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_rd_int_d
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_q_int
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_q_reg
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_q_valid
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_q
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_d
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_flush
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_flush_p2
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/state
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/seq_no
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/count
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/ser_count
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/word_count
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/total_words
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/timeout_counter
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/pack_data
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/fsm_out
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/escaper
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/fab_src
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/fsm_escape
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/fsm_escape_enable
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/crc_en
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/crc_en_masked
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/crc_reset
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/crc_value
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_almost_empty
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tx_almost_full
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/buf_frame_count_inc_ref
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/buf_frame_count_dec_sys
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/buf_frame_count
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tag_cycles
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tag_valid
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tag_valid_latched
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/tag_error
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/link_ok_delay_cnt
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/link_ok_delay_expired
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/link_ok_delay_expired_ref
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/link_ok_ref
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/clk_data
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/rst_n_ref
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/stamper_pulse_a
add wave -noupdate -group Streamers -expand -group tx /main/DUT/gen_nonempty_design/cmp_xwr_streamers/gen_tx/U_TX/rst_int_n
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/clk_i
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/rst_n_i
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/up_slave_i
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/up_slave_o
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/up_master_i
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/up_master_o
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/dn_i
add wave -noupdate -group IUart -expand -group top -expand /main/DUT/gen_nonempty_design/U_IUART/dn_o
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/gp_ctrl_o
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/iuart_up_out
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/iuart_wb_in
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/iuart_wb_out
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/uart_wb_in
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/uart_wb_out
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/uart_wb_out_r
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/rdr_rack
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/regs_in
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/regs_out
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/resized_addr
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_tx_din
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_tx_wr
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_tx_dout
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_tx_rd
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_tx_rd_r
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_tx_empty
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_tx_full
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_tx_count
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_tx_dout_m0
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_tx_dout_m0_r
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_tx_dout_m1
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_tx_dout_m1_r
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_tx_dout_m2
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_tx_dout_m2_r
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_tx_dout_esc
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_tx_dout_end
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_tx_end_seq
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_txreg_valid
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_txreg_valid_r
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_txreg_data
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_txreg_data_r
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_txreg_last
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_txreg_last_r
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_txreg_ready
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/txdata
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/txdata_valid
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/txdata_ready
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_rx_din
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_rx_wr
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_rx_dout
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_rx_rd
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_rx_rd_r
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_rx_empty
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_rx_full
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_rx_count
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_rx_countp1
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_rx_ovfl
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_rx_ecnt
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/fifo_rx_ecnt_r
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/wb_access
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/wb_access_r
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/wb_access_ready
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/wb_access_ready_r
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/wb_addr
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/wb_data
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/wb_sel
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/wb_wr
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/wb_dout_valid
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/wb_dout
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/uart_tx_bsy
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/uart_tx_bsy_r
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/uart_rx_rdy
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/uart_rx_rdy_r
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/uart_rx_data
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/uart_rx_data_r
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/access_uart_en
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/access_uart_en_r
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/access_uart
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/access_uart_r
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/access_fin
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/rddata_valid
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/rddata_valid_r
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/rddata
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/rddata_r
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/da_din
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/da_din_valid
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/cpl_data_valid
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/cpl_data
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/cpl_data_last
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/cpl_data_ready
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/wait_cycle
add wave -noupdate -group IUart -expand -group top /main/DUT/gen_nonempty_design/U_IUART/gp_ctrl
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/clk_i
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/rst_n_i
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/up_master_i
add wave -noupdate -group IUart -group DA -expand /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/up_master_o
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/din_i
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/din_valid_i
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/din_ready_o
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/cpl_data_valid_o
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/cpl_data_o
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/cpl_data_last_o
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/cpl_data_ready_i
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/esc_char_i
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/start_write_char_i
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/start_read_char_i
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/start_cpl_char_i
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/end_char_i
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/c_addr_add
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/char_raw
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/char_raw_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/char_valid
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/char_valid_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/char_filt
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/char_filt_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/din_valid_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/filt_is_start
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/filt_is_end
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/start_wb_wr
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/start_wb_rd
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/char_was_esc
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/char_was_esc_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/char_is_esc
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/char_is_esc_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/char_is_esc_val
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/char_is_start
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/char_is_start_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/char_is_end
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/char_is_end_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/state_is_start
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/state_is_size
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/state_is_addr0
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/state_is_addr1
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/state_is_addr2
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/state_is_addr3
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/state_is_data0
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/state_is_data1
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/state_is_data2
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/state_is_data3
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/state_is_end
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/state_is_csum
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/state_is_csum_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/addr
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/addr_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/data
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/data_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/csum
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/csum_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/csum_good
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/wb_sfifo_valid
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/wb_sfifo_data
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/wb_sfifo_user
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/wb_sfifo_last
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/wb_sfifo_ready
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/wb_sfifo_set
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/wb_sfifo_rst
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/wb_mfifo_valid
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/wb_mfifo_data
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/wb_mfifo_ready
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/wb_maccess_dout
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/wb_maccess_dout_valid
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/wb_maccess_dout_err
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/state_advance
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/state_advance_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/state
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/state_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/inseq
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/inseq_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/inseq_wb_wr
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/inseq_wb_wr_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/inseq_wb_rd
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/inseq_wb_rd_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/wb_dcyc_cnt
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/wb_dcyc_cnt_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/cpl_state
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/cpl_state_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/wb_cpl_done
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/wb_cpl_data_valid
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/wb_cpl_data_valid_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/wb_cpl_data
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/wb_cpl_data_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/wb_cpl_data_err
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/wb_cpl_data_err_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/state_may_extend
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/state_extends
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/state_extends_last
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/state_extends_last_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/cpl_state_advance
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/cpl_command
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/cpl_size
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/cpl_csum
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/cpl_csum_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/fifo_tx_din_is_esc
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/fifo_tx_din
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/fifo_tx_wr
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/fifo_tx_dout
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/fifo_tx_rd
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/fifo_tx_rd_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/fifo_tx_empty
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/fifo_tx_full
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/fifo_tx_count
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/cpl_reg_valid
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/cpl_reg_valid_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/cpl_reg_data
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/cpl_reg_data_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/cpl_reg_last
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/cpl_reg_last_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/cpl_reg_data_m1
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/cpl_reg_data_m1_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/cpl_reg_data_m2
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/cpl_reg_data_m2_r
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/cpl_reg_data_esc
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/cpl_reg_data_end
add wave -noupdate -group IUart -group DA /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/ones
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/clk_i
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/rst_n_i
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/mwb_o
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/mwb_i
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/wb_valid_i
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/wb_ready_o
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/wb_din_i
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/wb_addr_i
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/wb_sel_i
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/wb_wr_i
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/wb_dout_o
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/wb_dout_err_o
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/wb_dout_valid_o
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/mwb_out
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/mwb_out_r
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/wb_load_req
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/wb_load_req_r
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/wb_store_req
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/wb_store_req_r
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/wb_access
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/wb_access_r
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/wb_access_err
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/wb_access_end
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/wb_store_done
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/wb_load_done
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/wb_wait_cycle
add wave -noupdate -group MAC /main/DUT/gen_nonempty_design/U_IUART/U_DIRECT_Access/U_WB_MAccess/wb_cyc_taken
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/clk_i
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/rst_n_i
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/tx_data_o
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/tx_valid_o
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/tx_dreq_i
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/tx_last_p1_o
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/tx_flush_p1_o
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/rx_data_i
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/rx_valid_i
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/rx_first_p1_i
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/rx_dreq_o
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/rx_last_p1_i
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/rx_FrameHeader_o
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/rx_RFmFramePayloads_o
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/rx_RFsFramePayloads_o
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/rx_PFramePayloads_o
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/rx_Frame_valid_pX_o
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/rx_Frame_typeID_o
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/ready_o
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/tx_TransmitFrame_p1_i
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/tx_FrameHeader_i
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/tx_RFmFramePayloads_i
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/tx_RFsFramePayloads_i
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/tx_PFramePayloads_i
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/wb_slave_i
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/wb_slave_o
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/send_RF_frame
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/send_tick_p
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/wb_in_tx_ctrl
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/wb_in
add wave -noupdate -group RFTx -expand /main/DUT/gen_nonempty_design/cmp_RF/wb_out
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/rx_Frame_valid_p1
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/rx_Frame_valid_p1_unmasked
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/rx_FrameHeader
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/rx_RFmFramePayloads
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/rx_RFsFramePayloads
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/rx_PFramePayloads
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/s_out_state
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/output_cnt
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/rx_data_time_valid
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/rx_data_time_delay
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/rx_valid_pol_inv
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/tx_period_value
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/sample_rate_cnt
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/tx_data
add wave -noupdate -group RFTx /main/DUT/gen_nonempty_design/cmp_RF/tx_valid
add wave -noupdate -group RstGen /main/DUT/gen_nonempty_design/U_REF_Reset/free_clk_i
add wave -noupdate -group RstGen /main/DUT/gen_nonempty_design/U_REF_Reset/locked_i
add wave -noupdate -group RstGen /main/DUT/gen_nonempty_design/U_REF_Reset/clks_i
add wave -noupdate -group RstGen /main/DUT/gen_nonempty_design/U_REF_Reset/rstn_o
add wave -noupdate -group RstGen /main/DUT/gen_nonempty_design/U_REF_Reset/shifters
add wave -noupdate -group RstGen /main/DUT/gen_nonempty_design/U_REF_Reset/locked_count
add wave -noupdate -group RstGen /main/DUT/gen_nonempty_design/U_REF_Reset/master_rstn
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/slave_clk_i
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/slave_rst_n_i
add wave -noupdate -group Sys2DSP -expand /main/DUT/gen_nonempty_design/U_Sys2DSPClock/slave_i
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/slave_o
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/master_clk_i
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/master_rst_n_i
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/master_i
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/master_o
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/slave_ready_o
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/slave_stall_i
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/msend
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/mrecv
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/msend_vect
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/mrecv_vect
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/mw_en
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/mr_empty
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/mr_en
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/ssend
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/srecv
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/ssend_vect
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/srecv_vect
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/sw_en
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/sr_empty
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/sr_en
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/slave_CYC
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/master_o_STB
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/slave_o_PUSH
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/mpushed
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/mpopped
add wave -noupdate -group Sys2DSP /main/DUT/gen_nonempty_design/U_Sys2DSPClock/full
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/clk_sys_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/clk_dmtd_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/clk_ref_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/clk_aux_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/clk_ext_mul_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/clk_ext_mul_locked_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/clk_ext_stopped_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/clk_ext_rst_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/clk_ext_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/pps_ext_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/rst_n_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/dac_hpll_load_p1_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/dac_hpll_data_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/dac_dpll_load_p1_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/dac_dpll_data_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy_ref_clk_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy_tx_data_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy_tx_k_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy_tx_disparity_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy_tx_enc_err_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy_rx_data_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy_rx_rbclk_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy_rx_k_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy_rx_enc_err_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy_rx_bitslide_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy_rst_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy_rdy_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy_loopen_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy_loopen_vec_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy_tx_prbs_sel_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy_sfp_tx_fault_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy_sfp_los_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy_sfp_tx_disable_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy_rx_rbclk_sampled_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy_lpc_stat_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy_lpc_ctrl_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy8_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy8_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy16_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy16_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/led_act_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/led_link_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/scl_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/scl_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/sda_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/sda_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/sfp_scl_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/sfp_scl_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/sfp_sda_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/sfp_sda_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/sfp_det_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/btn1_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/btn2_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/spi_sclk_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/spi_ncs_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/spi_mosi_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/spi_miso_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/uart_rxd_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/uart_txd_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/owr_pwren_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/owr_en_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/owr_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/wb_adr_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/wb_dat_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/wb_dat_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/wb_sel_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/wb_we_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/wb_cyc_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/wb_stb_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/wb_ack_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/wb_err_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/wb_rty_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/wb_stall_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/aux_adr_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/aux_dat_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/aux_dat_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/aux_sel_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/aux_we_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/aux_cyc_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/aux_stb_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/aux_ack_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/aux_stall_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ext_snk_adr_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ext_snk_dat_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ext_snk_sel_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ext_snk_cyc_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ext_snk_we_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ext_snk_stb_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ext_snk_ack_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ext_snk_err_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ext_snk_stall_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ext_src_adr_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ext_src_dat_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ext_src_sel_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ext_src_cyc_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ext_src_stb_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ext_src_we_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ext_src_ack_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ext_src_err_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ext_src_stall_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/txtsu_port_id_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/txtsu_frame_id_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/txtsu_ts_value_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/txtsu_ts_incorrect_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/txtsu_stb_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/txtsu_ack_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/abscal_txts_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/abscal_rxts_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/fc_tx_pause_req_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/fc_tx_pause_delay_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/fc_tx_pause_ready_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/tm_link_up_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/tm_dac_value_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/tm_dac_wr_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/tm_clk_aux_lock_en_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/tm_clk_aux_locked_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/tm_time_valid_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/tm_tai_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/tm_cycles_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/pps_csync_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/pps_valid_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/pps_p_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/pps_led_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/rst_aux_n_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/link_ok_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/aux_diag_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/aux_diag_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/rst_wrc_n
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/rst_net_n
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/rst_net_resync_ref_n
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/rst_net_resync_ext_n
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/rst_net_resync_dmtd_n
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/rst_net_resync_rxclk_n
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/rst_net_resync_txclk_n
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/s_pps_csync
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/pps_valid
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ppsg_link_ok
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ppsg_wb_in
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ppsg_wb_out
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy_rx_clk
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy_tx_clk
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/spll_wb_in
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/spll_wb_out
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ep_txtsu_port_id
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ep_txtsu_frame_id
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ep_txtsu_ts_value
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ep_txtsu_ts_incorrect
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ep_txtsu_stb
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ep_txtsu_ack
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ep_led_link
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/phy_rst
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/mnic_mem_data_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/mnic_mem_addr_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/mnic_mem_wr_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/mnic_txtsu_ack
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/mnic_txtsu_stb
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/dpram_wbb_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/periph_slave_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/periph_slave_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/sysc_in_regs
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/sysc_out_regs
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/secbar_master_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/secbar_master_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/cbar_slave_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/cbar_slave_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/cbar_master_i
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/cbar_master_o
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ext_wb_in
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ext_wb_out
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/hpll_auxout
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/dmpll_auxout
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/clk_ref_slv
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/clk_rx_slv
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/s_dummy_addr
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/softpll_irq
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/lm32_irq_slv
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ep_wb_in
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ep_wb_out
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/minic_wb_in
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/minic_wb_out
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ep_src_out
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ep_src_in
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ep_snk_out
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/ep_snk_in
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/mux_src_out
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/mux_src_in
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/mux_snk_out
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/mux_snk_in
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/mux_class
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/spll_out_locked
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/dac_dpll_data
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/dac_dpll_sel
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/dac_dpll_load_p1
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/clk_fb
add wave -noupdate -group WrC /main/DUT/gen_nonempty_design/U_WR_CORE/WRPC/out_enable
add wave -noupdate -group Fpgen /main/DUT/U_DDSSyncUnit/clk_sys_i
add wave -noupdate -group Fpgen /main/DUT/U_DDSSyncUnit/clk_ref_i
add wave -noupdate -group Fpgen /main/DUT/U_DDSSyncUnit/rst_sys_n_i
add wave -noupdate -group Fpgen /main/DUT/U_DDSSyncUnit/clk_ser_ext_i
add wave -noupdate -group Fpgen /main/DUT/U_DDSSyncUnit/ext_trigger_p_i
add wave -noupdate -group Fpgen /main/DUT/U_DDSSyncUnit/pps_p_i
add wave -noupdate -group Fpgen /main/DUT/U_DDSSyncUnit/pulse_o
add wave -noupdate -group Fpgen /main/DUT/U_DDSSyncUnit/clk_par_o
add wave -noupdate -group Fpgen /main/DUT/U_DDSSyncUnit/slave_i
add wave -noupdate -group Fpgen /main/DUT/U_DDSSyncUnit/slave_o
add wave -noupdate -group Fpgen /main/DUT/U_DDSSyncUnit/ch
add wave -noupdate -group Fpgen /main/DUT/U_DDSSyncUnit/clk_par
add wave -noupdate -group Fpgen /main/DUT/U_DDSSyncUnit/clk_ser
add wave -noupdate -group Fpgen /main/DUT/U_DDSSyncUnit/clk_odelay
add wave -noupdate -group Fpgen /main/DUT/U_DDSSyncUnit/regs_out
add wave -noupdate -group Fpgen /main/DUT/U_DDSSyncUnit/regs_in
add wave -noupdate -group Fpgen /main/DUT/U_DDSSyncUnit/rst_n_wr
add wave -noupdate -group Fpgen /main/DUT/U_DDSSyncUnit/pps_p_d
add wave -noupdate -group Fpgen /main/DUT/U_DDSSyncUnit/pps_ext
add wave -noupdate -group Fpgen /main/DUT/U_DDSSyncUnit/pps_cnt
add wave -noupdate -group Fpgen /main/DUT/U_DDSSyncUnit/pll_locked
add wave -noupdate -group Fpgen /main/DUT/U_DDSSyncUnit/rst_serdes
add wave -noupdate -group fpg0 /main/DUT/U_DDSSyncUnit/gen_channels(0)/gen_is_kintex7_pg/U_Pulse_Gen/clk_par_i
add wave -noupdate -group fpg0 /main/DUT/U_DDSSyncUnit/gen_channels(0)/gen_is_kintex7_pg/U_Pulse_Gen/clk_serdes_i
add wave -noupdate -group fpg0 /main/DUT/U_DDSSyncUnit/gen_channels(0)/gen_is_kintex7_pg/U_Pulse_Gen/rst_serdes_i
add wave -noupdate -group fpg0 /main/DUT/U_DDSSyncUnit/gen_channels(0)/gen_is_kintex7_pg/U_Pulse_Gen/rst_sys_n_i
add wave -noupdate -group fpg0 /main/DUT/U_DDSSyncUnit/gen_channels(0)/gen_is_kintex7_pg/U_Pulse_Gen/cont_i
add wave -noupdate -group fpg0 /main/DUT/U_DDSSyncUnit/gen_channels(0)/gen_is_kintex7_pg/U_Pulse_Gen/pol_i
add wave -noupdate -group fpg0 /main/DUT/U_DDSSyncUnit/gen_channels(0)/gen_is_kintex7_pg/U_Pulse_Gen/coarse_i
add wave -noupdate -group fpg0 /main/DUT/U_DDSSyncUnit/gen_channels(0)/gen_is_kintex7_pg/U_Pulse_Gen/trig_p_i
add wave -noupdate -group fpg0 /main/DUT/U_DDSSyncUnit/gen_channels(0)/gen_is_kintex7_pg/U_Pulse_Gen/pulse_o
add wave -noupdate -group fpg0 /main/DUT/U_DDSSyncUnit/gen_channels(0)/gen_is_kintex7_pg/U_Pulse_Gen/dly_load_i
add wave -noupdate -group fpg0 /main/DUT/U_DDSSyncUnit/gen_channels(0)/gen_is_kintex7_pg/U_Pulse_Gen/dly_fine_i
add wave -noupdate -group fpg0 /main/DUT/U_DDSSyncUnit/gen_channels(0)/gen_is_kintex7_pg/U_Pulse_Gen/par_data
add wave -noupdate -group fpg0 /main/DUT/U_DDSSyncUnit/gen_channels(0)/gen_is_kintex7_pg/U_Pulse_Gen/dout_predelay
add wave -noupdate -group fpg0 /main/DUT/U_DDSSyncUnit/gen_channels(0)/gen_is_kintex7_pg/U_Pulse_Gen/dout_prebuf
add wave -noupdate -group fpg0 /main/DUT/U_DDSSyncUnit/gen_channels(0)/gen_is_kintex7_pg/U_Pulse_Gen/dout_nodelay
add wave -noupdate -group fpg0 /main/DUT/U_DDSSyncUnit/gen_channels(0)/gen_is_kintex7_pg/U_Pulse_Gen/odelay_load
add wave -noupdate -group fpg0 /main/DUT/U_DDSSyncUnit/gen_channels(0)/gen_is_kintex7_pg/U_Pulse_Gen/rst
add wave -noupdate -group fpg0 /main/DUT/U_DDSSyncUnit/gen_channels(0)/gen_is_kintex7_pg/U_Pulse_Gen/odelay_ntaps
add wave -noupdate -group fpg0 /main/DUT/U_DDSSyncUnit/gen_channels(0)/gen_is_kintex7_pg/U_Pulse_Gen/trig_d
add wave -noupdate -group fpg0 /main/DUT/U_DDSSyncUnit/gen_channels(0)/gen_is_kintex7_pg/U_Pulse_Gen/mask
add wave -noupdate -group fpg0 /main/DUT/U_DDSSyncUnit/gen_channels(0)/gen_is_kintex7_pg/U_Pulse_Gen/flip
add wave -noupdate -group fpg0 /main/DUT/U_DDSSyncUnit/gen_channels(0)/gen_is_kintex7_pg/U_Pulse_Gen/dly_load_d
add wave -noupdate -expand -group WbS /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/rst_n_i
add wave -noupdate -expand -group WbS /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/clk_sys_i
add wave -noupdate -expand -group WbS /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/rdr_rack_o
add wave -noupdate -expand -group WbS /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/slave_i
add wave -noupdate -expand -group WbS /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/slave_o
add wave -noupdate -expand -group WbS /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/int_o
add wave -noupdate -expand -group WbS /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/regs_i
add wave -noupdate -expand -group WbS -expand /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/regs_o
add wave -noupdate -expand -group WbS /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/iuart_uart_en_int
add wave -noupdate -expand -group WbS /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/iuart_esc_char_int
add wave -noupdate -expand -group WbS /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/iuart_start_insn_char_int
add wave -noupdate -expand -group WbS /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/iuart_start_write_char_int
add wave -noupdate -expand -group WbS /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/iuart_start_read_char_int
add wave -noupdate -expand -group WbS /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/iuart_start_cpl_char_int
add wave -noupdate -expand -group WbS /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/iuart_end_char_int
add wave -noupdate -expand -group WbS /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/iuart_write_ctrl_char_int
add wave -noupdate -expand -group WbS /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/iuart_read_ctrl_char_int
add wave -noupdate -expand -group WbS /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/ack_sreg
add wave -noupdate -expand -group WbS /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/rddata_reg
add wave -noupdate -expand -group WbS /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/wrdata_reg
add wave -noupdate -expand -group WbS /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/bwsel_reg
add wave -noupdate -expand -group WbS /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/rwaddr_reg
add wave -noupdate -expand -group WbS /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/ack_in_progress
add wave -noupdate -expand -group WbS /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/wr_int
add wave -noupdate -expand -group WbS /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/rd_int
add wave -noupdate -expand -group WbS /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/allones
add wave -noupdate -expand -group WbS /main/DUT/gen_nonempty_design/U_IUART/U_WB_SLAVE/allzeros
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {241033944200 fs} 0} {{Cursor 2} {8313060 fs} 0}
configure wave -namecolwidth 324
configure wave -valuecolwidth 84
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 fs} {33554432 ps}
