sim_tool = "modelsim"
top_module="main"
action = "simulation"
target = "xilinx"
fetchto = "../../ip_cores"
vcom_opt="-mixedsvvh l -2008"
sim_top="main"
syn_device="xc7k70t"
include_dirs=["../../ip_cores/wr-cores/sim", "../../ip_cores/general-cores/modules/wishbone/wb_lm32/platform/generic", "../../ip_cores/general-cores/modules/wishbone/wb_lm32/src", "../include" ]

files = [ "main.sv" ]

modules = { "local" :  [ "../../rtl" , "../../top/ertm14" ] }

